/**
 * @file tests/dsde/models.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTS_DSDE_MODELS_HPP
#define TESTS_DSDE_MODELS_HPP

#include <artis-star/common/time/DoubleTime.hpp>
#include <artis-star/common/utils/Trace.hpp>

#include <artis-star/kernel/dsde/Coordinator.hpp>
#include <artis-star/kernel/dsde/Executive.hpp>
#include <artis-star/kernel/pdevs/Dynamics.hpp>

#include <tests/dsde/graph_manager.hpp>

namespace artis {
namespace tests {
namespace dsde {

class Beep :
    public artis::pdevs::Dynamics<common::DoubleTime, Beep>
{
public:
  enum inputs
  {
    IN
  };
  enum outputs
  {
    OUT
  };

  Beep(const std::string &name,
       const artis::pdevs::Context<common::DoubleTime, Beep, artis::common::NoParameters> &context)
      :
      artis::pdevs::Dynamics<common::DoubleTime, Beep>(name, context), _value(0)
  {
    input_ports({{IN, "in"}});
    output_ports({{OUT, "out"}});
  }

  ~Beep() override = default;

  void dint(const typename common::DoubleTime::type &t) override
  {

#ifndef WITH_TRACE
    (void)t;
#endif

#ifdef WITH_TRACE
    common::Trace<common::DoubleTime>::trace()
        << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                    common::FormalismType::PDEVS,
                                                    common::FunctionType::DELTA_INT,
                                                    common::LevelType::USER);
    common::Trace<common::DoubleTime>::trace().flush();
#endif

    if (_phase == SEND or _phase == INIT) {
      _phase = WAIT;
    }
  }

  void
  dext(const typename common::DoubleTime::type &t,
       const typename common::DoubleTime::type & /* e */,
       const common::event::Bag <common::DoubleTime> &bag) override
  {

#ifndef WITH_TRACE
    (void)t;
    (void)bag;
#endif

#ifdef WITH_TRACE
    common::Trace<common::DoubleTime>::trace()
        << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                    common::FormalismType::PDEVS,
                                                    common::FunctionType::DELTA_EXT,
                                                    common::LevelType::USER)
        << "messages = " << bag.to_string();
    common::Trace<common::DoubleTime>::trace().flush();
#endif

    _phase = SEND;
  }

  void dconf(const typename common::DoubleTime::type &t,
             const typename common::DoubleTime::type & /* e */,
             const common::event::Bag <common::DoubleTime> &bag) override
  {

#ifndef WITH_TRACE
    (void)t;
    (void)bag;
#endif

#ifdef WITH_TRACE
    common::Trace<common::DoubleTime>::trace()
        << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                    common::FormalismType::PDEVS,
                                                    common::FunctionType::DELTA_CONF,
                                                    common::LevelType::USER)
        << "messages = " << bag.to_string();
    common::Trace<common::DoubleTime>::trace().flush();
#endif

  }

  void
  start(const typename common::DoubleTime::type &t) override
  {

#ifndef WITH_TRACE
    (void)t;
#endif

#ifdef WITH_TRACE
    common::Trace<common::DoubleTime>::trace()
        << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                    common::FormalismType::PDEVS,
                                                    common::FunctionType::START,
                                                    common::LevelType::USER);
    common::Trace<common::DoubleTime>::trace().flush();
#endif

    _phase = INIT;
  }

  typename common::DoubleTime::type
  ta(const typename common::DoubleTime::type &t) const override
  {

#ifndef WITH_TRACE
    (void)t;
#endif

#ifdef WITH_TRACE
    common::Trace<common::DoubleTime>::trace()
        << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                    common::FormalismType::PDEVS,
                                                    common::FunctionType::TA,
                                                    common::LevelType::USER);
    common::Trace<common::DoubleTime>::trace().flush();
#endif

    if (_phase == WAIT) {
      return (rand() % 10) / 2. + 1;
    } else {
      return 0;
    }
  }

  common::event::Bag <common::DoubleTime>
  lambda(const typename common::DoubleTime::type &t) const override
  {

#ifndef WITH_TRACE
    (void)t;
#endif
    common::event::Bag<common::DoubleTime> bag;

    bag.push_back(artis::common::event::ExternalEvent<common::DoubleTime>(OUT, _value));

#ifdef WITH_TRACE
    common::Trace<common::DoubleTime>::trace()
        << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                    common::FormalismType::PDEVS,
                                                    common::FunctionType::LAMBDA,
                                                    common::LevelType::USER)
        << "messages = " << bag.to_string();
    common::Trace<common::DoubleTime>::trace().flush();
#endif

    return bag;
  }

private:
  enum Phase
  {
    INIT, WAIT, SEND
  };

  Phase _phase;
  double _value;
};

}
}
} // namespace artis tests dsde

#endif
