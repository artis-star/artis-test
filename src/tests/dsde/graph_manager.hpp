/**
 * @file tests/dsde/graph_manager.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTS_DSDE_GRAPH_MANAGER_HPP
#define TESTS_DSDE_GRAPH_MANAGER_HPP

#include <artis-star/kernel/dsde/Coordinator.hpp>
#include <artis-star/kernel/dsde/GraphManager.hpp>
#include <artis-star/kernel/pdevs/Simulator.hpp>

#include <tests/dsde/models.hpp>

namespace artis {
namespace tests {
namespace dsde {

class FlatGraphManager :
    public artis::dsde::GraphManager<common::DoubleTime>
{
public:
  enum submodels
  {
    BEEP = 1
  };

  FlatGraphManager(common::Coordinator<common::DoubleTime> *coordinator,
                   const artis::common::NoParameters &parameters,
                   const artis::common::NoParameters &graph_parameters)
      :
      artis::dsde::GraphManager<common::DoubleTime>(coordinator, parameters,
                                                    graph_parameters),
      _beep("beep", parameters)
  {
    add_child(BEEP, &_beep);
  }

  ~FlatGraphManager() override = default;

private:
  artis::pdevs::Simulator<common::DoubleTime, Beep> _beep;
};

class Executive :
    public artis::dsde::Executive<common::DoubleTime, Executive>
{
public:
  Executive(
      const artis::dsde::ExecutiveContext<common::DoubleTime,
                                          Executive,
                                          artis::common::NoParameters,
                                          artis::common::NoParameters> &context,
      artis::dsde::GraphManager<common::DoubleTime,
                                artis::common::NoParameters,
                                artis::common::NoParameters> &graph_manager)
      :
      artis::dsde::Executive<common::DoubleTime, Executive>(context,
                                                            graph_manager), _index(0), _up(true)
  {}

  ~Executive() override = default;

  void dint(typename common::DoubleTime::type t) override
  {

#ifndef WITH_TRACE
    (void)t;
#endif

#ifdef WITH_TRACE
    common::Trace<common::DoubleTime>::trace()
        << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                    common::FormalismType::DSDE,
                                                    common::FunctionType::DELTA_INT,
                                                    common::LevelType::USER);
    common::Trace<common::DoubleTime>::trace().flush();
#endif

    if (_up) {
      ++_index;
    } else {
      --_index;
    }
    if (_index == 4 and _up) {
      _up = false;
      --_index;
    }
    if (_up) {
      graph_manager().add_dynamic_child(_index,
                                        new artis::pdevs::Simulator<common::DoubleTime, Beep>(
                                            "beep" + std::to_string(_index),
                                            artis::common::NoParameters()));
      graph_manager().add_link(FlatGraphManager::BEEP, Beep::OUT, _index,
                               Beep::IN);
    } else {
      if (_index == 3) {
        graph_manager().remove_dynamic_child(t, _index);
      } else if (_index == 2) {
        graph_manager().remove_link(FlatGraphManager::BEEP, Beep::OUT, _index,
                                    Beep::IN);
      }
    }
  }

  typename common::DoubleTime::type
  start(typename common::DoubleTime::type t) override
  {

#ifndef WITH_TRACE
    (void)t;
#endif

#ifdef WITH_TRACE
    common::Trace<common::DoubleTime>::trace()
        << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                    common::FormalismType::DSDE,
                                                    common::FunctionType::START,
                                                    common::LevelType::USER);
    common::Trace<common::DoubleTime>::trace().flush();
#endif

    _index = 1;
    _up = true;
    return 1;
  }

  typename common::DoubleTime::type
  ta(typename common::DoubleTime::type t) const override
  {
#ifndef WITH_TRACE
    (void)t;
#endif

#ifdef WITH_TRACE
    common::Trace<common::DoubleTime>::trace()
        << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                    common::FormalismType::DSDE,
                                                    common::FunctionType::TA,
                                                    common::LevelType::USER);
    common::Trace<common::DoubleTime>::trace().flush();
#endif

    return 2;
  }

private:
  int _index;
  bool _up;
};

}
}
} // namespace artis tests dsde

#endif
