/**
 * @file tests/dsde/tests.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <tests/dsde/graph_manager.hpp>
#include <tests/dsde/models.hpp>

#include <artis-star/common/RootCoordinator.hpp>

#define CATCH_CONFIG_MAIN

#include <catch2/catch_all.hpp>

using namespace artis::tests::dsde;
using namespace artis::common;

TEST_CASE("dsde/flat", "run")
{
  artis::common::context::Context<artis::common::DoubleTime> context(0, 10);
  artis::common::RootCoordinator<
      DoubleTime, artis::dsde::Coordinator<
          DoubleTime,
          artis::tests::dsde::FlatGraphManager,
          artis::tests::dsde::Executive>
  > rc(context, "root");

  artis::common::Trace<DoubleTime>::trace().clear();
  rc.run(context);

  REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().filter_model_name(
      "beep").filter_function_type(artis::common::FunctionType::START).filter_time(0).size()
              == 1);
  REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().filter_model_name(
      "beep2").filter_function_type(artis::common::FunctionType::START).filter_time(1).size()
              == 1);
  REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().filter_model_name(
      "beep3").filter_function_type(artis::common::FunctionType::START).filter_time(3).size()
              == 1);

  REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().filter_model_name(
      "beep2").filter_function_type(artis::common::FunctionType::DELTA_INT).filter_time(
      1).size()
              == 1);
  REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().filter_model_name(
      "beep2").filter_function_type(artis::common::FunctionType::TA).filter_time(1).size()
              == 2);

  REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().filter_model_name(
      "beep2").filter_function_type(artis::common::FunctionType::DELTA_INT).filter_time(
      2.5).size()
              == 1);
  REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().filter_model_name(
      "beep2").filter_function_type(artis::common::FunctionType::DELTA_EXT).filter_time(
      2.5).size()
              == 1);
  REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().filter_model_name(
      "beep2").filter_function_type(artis::common::FunctionType::TA).filter_time(2.5).size()
              == 2);
  REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().filter_model_name(
      "beep2").filter_function_type(artis::common::FunctionType::LAMBDA).filter_time(2.5).size()
              == 1);

  REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().filter_model_name(
      "beep2").filter_function_type(artis::common::FunctionType::DELTA_INT).filter_time(
      6).size()
              == 1);
  REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().filter_model_name(
      "beep2").filter_function_type(artis::common::FunctionType::TA).filter_time(6).size()
              == 1);
  REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().filter_model_name(
      "beep2").filter_function_type(artis::common::FunctionType::LAMBDA).filter_time(6).size()
              == 1);

  REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().filter_model_name(
      "beep3").filter_function_type(artis::common::FunctionType::DELTA_INT).filter_time(
      3).size()
              == 1);
  REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().filter_model_name(
      "beep3").filter_function_type(artis::common::FunctionType::TA).filter_time(3).size()
              == 2);
  REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().filter_model_name(
      "beep3").filter_function_type(artis::common::FunctionType::LAMBDA).filter_time(3).size()
              == 1);

  REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().filter_model_name(
      "beep3").filter_function_type(artis::common::FunctionType::FINISH).filter_time(5).size()
              == 1);
}