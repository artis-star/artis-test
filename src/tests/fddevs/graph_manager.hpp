/**
 * @file tests/fddevs/graph_manager.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTS_FDDEVS_GRAPH_MANAGER_HPP
#define TESTS_FDDEVS_GRAPH_MANAGER_HPP

#include <tests/fddevs/models.hpp>

#include <artis-star/kernel/fddevs/Coordinator.hpp>
#include <artis-star/kernel/fddevs/GraphManager.hpp>
#include <artis-star/kernel/fddevs/Simulator.hpp>

namespace artis {
namespace tests {
namespace fddevs {

class FlatGraphManager :
    public artis::fddevs::GraphManager<common::DoubleTime>
{
public:
  enum submodels
  {
    M_BEEP, M_CRC, M_MXR,
  };

  FlatGraphManager(common::Coordinator<common::DoubleTime> *coordinator,
                   const artis::common::NoParameters &parameters,
                   const artis::common::NoParameters &graph_parameters)
      :
      artis::fddevs::GraphManager<common::DoubleTime>(coordinator, parameters, graph_parameters),
      _beep("beep", parameters), _crc("crc", parameters), _mxr("mxr", parameters)
  {
    add_child(M_BEEP, &_beep);
    add_child(M_CRC, &_crc);
    add_child(M_MXR, &_mxr);

    out({&_beep, Beep::OUT_P}) >> in({&_crc, CRC::IN_P});
    out({&_crc, CRC::OUT_G}) >> in({&_mxr, MXR::IN_A});
    out({&_crc, CRC::OUT_W}) >> in({&_mxr, MXR::IN_B});
  }

  ~FlatGraphManager() override = default;

private:
  artis::fddevs::Simulator<common::DoubleTime, Beep> _beep;
  artis::fddevs::Simulator<common::DoubleTime, CRC> _crc;
  artis::fddevs::Simulator<common::DoubleTime, MXR> _mxr;
};

}
}
} // namespace artis tests fddevs

#endif
