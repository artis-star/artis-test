/**
 * @file tests/pdevs/tests.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <tests/fddevs/graph_manager.hpp>
#include <tests/fddevs/models.hpp>

#include <artis-star/common/RootCoordinator.hpp>

#define CATCH_CONFIG_MAIN

#include <catch2/catch_all.hpp>

#include <iostream>

using namespace artis::tests::fddevs;
using namespace artis::common;

TEST_CASE("fddevs/cross road system", "run")
{
  context::Context<DoubleTime> context(0, 70);
  RootCoordinator<
      DoubleTime, artis::fddevs::Coordinator<
          DoubleTime,
          FlatGraphManager>
  > rc(context, "root", NoParameters(), NoParameters());

  Trace<DoubleTime>::trace().clear();
  rc.run(context);

  std::cout << Trace<DoubleTime>::trace().elements().filter_model_name("crc").to_string()
            << std::endl;
  std::cout << Trace<DoubleTime>::trace().elements().filter_model_name("mxr").to_string()
            << std::endl;

  REQUIRE(Trace<DoubleTime>::trace().elements().filter_model_name("crc").filter_function_type(
      FunctionType::S_MESSAGE).size() == 18);
  REQUIRE(Trace<DoubleTime>::trace().elements().filter_model_name("crc").filter_function_type(
      FunctionType::S_MESSAGE)[0].get_comment() == ": BEFORE => tl = 0 ; tn = 0 ; S = 0");
  REQUIRE(Trace<DoubleTime>::trace().elements().filter_model_name("crc").filter_function_type(
      FunctionType::S_MESSAGE)[1].get_comment() == ": AFTER => tl = 0 ; tn = 0 ; S = 1");
  REQUIRE(Trace<DoubleTime>::trace().elements().filter_model_name("crc").filter_function_type(
      FunctionType::S_MESSAGE)[2].get_comment() == ": BEFORE => tl = 0 ; tn = 0 ; S = 1");
  REQUIRE(Trace<DoubleTime>::trace().elements().filter_model_name("crc").filter_function_type(
      FunctionType::S_MESSAGE)[3].get_comment() == ": AFTER => tl = 0 ; tn = 10 ; S = 2");
  REQUIRE(Trace<DoubleTime>::trace().elements().filter_model_name("crc").filter_function_type(
      FunctionType::S_MESSAGE)[4].get_comment() == ": BEFORE => tl = 0 ; tn = 10 ; S = 2");
  REQUIRE(Trace<DoubleTime>::trace().elements().filter_model_name("crc").filter_function_type(
      FunctionType::S_MESSAGE)[5].get_comment() == ": AFTER => tl = 10 ; tn = 20 ; S = 2");
  REQUIRE(Trace<DoubleTime>::trace().elements().filter_model_name("crc").filter_function_type(
      FunctionType::S_MESSAGE)[6].get_comment() == ": BEFORE => tl = 10 ; tn = 20 ; S = 2");
  REQUIRE(Trace<DoubleTime>::trace().elements().filter_model_name("crc").filter_function_type(
      FunctionType::S_MESSAGE)[7].get_comment() == ": AFTER => tl = 20 ; tn = 30 ; S = 2");
  REQUIRE(Trace<DoubleTime>::trace().elements().filter_model_name("crc").filter_function_type(
      FunctionType::S_MESSAGE)[8].get_comment() == ": BEFORE => tl = 20 ; tn = 30 ; S = 2");
  REQUIRE(Trace<DoubleTime>::trace().elements().filter_model_name("crc").filter_function_type(
      FunctionType::S_MESSAGE)[9].get_comment() == ": AFTER => tl = 27 ; tn = 32 ; S = 3");
  REQUIRE(Trace<DoubleTime>::trace().elements().filter_model_name("crc").filter_function_type(
      FunctionType::S_MESSAGE)[10].get_comment() == ": BEFORE => tl = 27 ; tn = 32 ; S = 3");
  REQUIRE(Trace<DoubleTime>::trace().elements().filter_model_name("crc").filter_function_type(
      FunctionType::S_MESSAGE)[11].get_comment() == ": AFTER => tl = 32 ; tn = 34 ; S = 4");
  REQUIRE(Trace<DoubleTime>::trace().elements().filter_model_name("crc").filter_function_type(
      FunctionType::S_MESSAGE)[12].get_comment() == ": BEFORE => tl = 32 ; tn = 34 ; S = 4");
  REQUIRE(Trace<DoubleTime>::trace().elements().filter_model_name("crc").filter_function_type(
      FunctionType::S_MESSAGE)[13].get_comment() == ": AFTER => tl = 34 ; tn = 60 ; S = 5");
  REQUIRE(Trace<DoubleTime>::trace().elements().filter_model_name("crc").filter_function_type(
      FunctionType::S_MESSAGE)[14].get_comment() == ": BEFORE => tl = 34 ; tn = 60 ; S = 5");
  REQUIRE(Trace<DoubleTime>::trace().elements().filter_model_name("crc").filter_function_type(
      FunctionType::S_MESSAGE)[15].get_comment() == ": AFTER => tl = 60 ; tn = 62 ; S = 6");
  REQUIRE(Trace<DoubleTime>::trace().elements().filter_model_name("crc").filter_function_type(
      FunctionType::S_MESSAGE)[16].get_comment() == ": BEFORE => tl = 60 ; tn = 62 ; S = 6");
  REQUIRE(Trace<DoubleTime>::trace().elements().filter_model_name("crc").filter_function_type(
      FunctionType::S_MESSAGE)[17].get_comment() == ": AFTER => tl = 62 ; tn = 72 ; S = 2");

  REQUIRE(Trace<DoubleTime>::trace().elements().filter_model_name("mxr").filter_function_type(
      FunctionType::S_MESSAGE).size() == 12);
  REQUIRE(Trace<DoubleTime>::trace().elements().filter_model_name("mxr").filter_function_type(
      FunctionType::POST_EVENT).size() == 12);

}