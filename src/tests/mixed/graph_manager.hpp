/**
 * @file tests/mixed/graph_manager.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTS_MIXED_GRAPH_MANAGER_HPP
#define TESTS_MIXED_GRAPH_MANAGER_HPP

#include <tests/mixed/models.hpp>

#include <artis-star/kernel/dtss/Coordinator.hpp>
#include <artis-star/kernel/dtss/GraphManager.hpp>
#include <artis-star/kernel/dtss/Policy.hpp>
#include <artis-star/kernel/dtss/Simulator.hpp>

#include <artis-star/kernel/pdevs/Coordinator.hpp>
#include <artis-star/kernel/pdevs/GraphManager.hpp>
#include <artis-star/kernel/pdevs/Simulator.hpp>

namespace artis {
namespace tests {
namespace mixed {

class S1GraphManager : public pdevs::GraphManager<common::DoubleTime> {
public:
  enum submodels {
    OneA, OneB
  };
  enum outputs {
    OUT
  };

  S1GraphManager(common::Coordinator<common::DoubleTime> *coordinator,
                 const artis::common::NoParameters &parameters,
                 const artis::common::NoParameters &graph_parameters)
    :
    pdevs::GraphManager<common::DoubleTime>(coordinator, parameters, graph_parameters),
    a("a1", parameters),
    b("b1", parameters) {
    add_child(OneA, &a);
    add_child(OneB, &b);

    coordinator->output_port({OUT, "out"});

    out({&a, A1::OUT}) >> in({&b, B1::IN});
    out({&b, B1::OUT}) >> out({coordinator, OUT});
  }

  ~S1GraphManager() override = default;

private:
  pdevs::Simulator<common::DoubleTime, A1, artis::common::NoParameters> a;
  pdevs::Simulator<common::DoubleTime, B1, artis::common::NoParameters> b;
};

class S2GraphManager :
  public dtss::GraphManager<common::DoubleTime, artis::dtss::Parameters<common::DoubleTime>> {
public:
  enum submodels {
    OneA, OneB
  };
  enum inputs {
    IN
  };

  S2GraphManager(common::Coordinator<common::DoubleTime> *coordinator,
                 const artis::dtss::Parameters<common::DoubleTime> &parameters,
                 const artis::common::NoParameters &graph_parameters)
    :
    dtss::GraphManager<common::DoubleTime, artis::dtss::Parameters<common::DoubleTime>>(
      coordinator, parameters, graph_parameters),
    a("a2", parameters, parameters.time_step), b("b2", parameters, parameters.time_step) {
    add_child(OneA, &a);
    add_child(OneB, &b);

    coordinator->input_port({IN, "in"});

    out({&a, A2::OUT}) >> in({&b, B2::IN});
    in({coordinator, IN}) >> in({&a, A2::IN});
  }

  ~S2GraphManager() override = default;

private:
  dtss::Simulator<common::DoubleTime, A2, artis::dtss::Parameters<common::DoubleTime>> a;
  dtss::Simulator<common::DoubleTime, B2, artis::dtss::Parameters<common::DoubleTime>> b;
};

class RootGraphManager : public pdevs::GraphManager<common::DoubleTime> {
public:
  enum submodels {
    OneS1, OneS2
  };

  RootGraphManager(common::Coordinator<common::DoubleTime> *coordinator,
                   const artis::common::NoParameters &parameters,
                   const artis::common::NoParameters &graph_parameters)
    :
    pdevs::GraphManager<common::DoubleTime>(coordinator, parameters, graph_parameters),
    S1("S1", artis::common::NoParameters(), artis::common::NoParameters()),
    S2("S2", {2}, artis::common::NoParameters()) {
    add_child(OneS1, &S1);
    add_child(OneS2, &S2);

    out({&S1, S1GraphManager::OUT}) >> in({&S2, S2GraphManager::IN});
  }

  ~RootGraphManager() override = default;

private:
  artis::pdevs::Coordinator<common::DoubleTime, S1GraphManager> S1;
  artis::dtss::Coordinator<
    common::DoubleTime,
    artis::dtss::LastBagPolicy,
    S2GraphManager,
    artis::dtss::Parameters<common::DoubleTime>> S2;
};

template<int size>
class LinearGraphManager : public pdevs::GraphManager<common::DoubleTime> {
public:
  LinearGraphManager(common::Coordinator<common::DoubleTime> *coordinator,
                     const artis::common::NoParameters &parameters,
                     const artis::common::NoParameters &graph_parameters)
    :
    pdevs::GraphManager<common::DoubleTime>(coordinator, parameters, graph_parameters) {
    for (unsigned int i = 1; i <= size; ++i) {
      std::ostringstream ss;

      ss << "a" << i;
      _models.push_back(
        new pdevs::Simulator<common::DoubleTime, Beep, artis::common::NoParameters>(
          ss.str(), artis::common::NoParameters()));
    }
    for (unsigned int i = 0; i < size; ++i) {
      LinearGraphManager<size>::add_child(i, _models[i]);
    }
    for (unsigned int i = 0; i < size - 1; ++i) {
      out({_models[i], Beep::OUT}) >> in({_models[i + 1], B1::IN});
    }
  }

  ~LinearGraphManager() override {
    for (unsigned int i = 0; i < size; ++i) {
      delete _models[i];
    }
  }

private:
  std::vector<pdevs::Simulator<common::DoubleTime, Beep, artis::common::NoParameters> *> _models;
};

class Linear2GraphManager : public pdevs::GraphManager<common::DoubleTime> {
public:
  enum inputs {
    IN
  };
  enum outputs {
    OUT
  };

  Linear2GraphManager(common::Coordinator<common::DoubleTime> *coordinator,
                      const artis::common::NoParameters &parameters,
                      const artis::common::NoParameters &graph_parameters)
    :
    pdevs::GraphManager<common::DoubleTime>(coordinator, parameters, graph_parameters) {
    for (unsigned int i = 1; i <= 100; ++i) {
      std::ostringstream ss;

      ss << "a" << i;
      _models.push_back(
        new pdevs::Simulator<common::DoubleTime, Beep, artis::common::NoParameters>(
          ss.str(), artis::common::NoParameters()));
    }
    for (unsigned int i = 0; i < 100; ++i) {
      add_child(i, _models[i]);
    }
    for (unsigned int i = 0; i < 99; ++i) {
      out({_models[i], Beep::OUT}) >> in({_models[i + 1], B1::IN});
    }

    coordinator->input_port({IN, "in"});
    coordinator->output_port({OUT, "out"});

    in({coordinator, IN}) >> in({_models[0], Beep::IN});
    out({_models[99], Beep::OUT}) >> out({coordinator, OUT});
  }

  ~Linear2GraphManager() override {
    for (unsigned int i = 0; i < 100; ++i) {
      delete _models[i];
    }
  }

private:
  std::vector<pdevs::Simulator<common::DoubleTime, Beep, artis::common::NoParameters> *> _models;
};

class Root2GraphManager : public pdevs::GraphManager<common::DoubleTime> {
public:
  enum submodels {
    OneS1, OneS2
  };

  Root2GraphManager(common::Coordinator<common::DoubleTime> *coordinator,
                    const artis::common::NoParameters &parameters,
                    const artis::common::NoParameters &graph_parameters
  )
    :
    pdevs::GraphManager<common::DoubleTime>(coordinator, parameters, graph_parameters),
    S1("S1", artis::common::NoParameters(), artis::common::NoParameters()),
    S2("S2", artis::common::NoParameters(), artis::common::NoParameters()) {
    add_child(OneS1, &S1);
    add_child(OneS2, &S2);

    out({&S1, S1GraphManager::OUT}) >> in({&S2, S2GraphManager::IN});
  }

  ~Root2GraphManager() override = default;

private:
  artis::pdevs::Coordinator<common::DoubleTime, Linear2GraphManager> S1;
  artis::pdevs::Coordinator<common::DoubleTime, Linear2GraphManager> S2;
};

class Root3GraphManager : public pdevs::GraphManager<common::DoubleTime> {
public:
  enum submodels {
    OneS1, OneS2
  };

  Root3GraphManager(common::Coordinator<common::DoubleTime> *coordinator,
                    const artis::common::NoParameters &parameters,
                    const artis::common::NoParameters &graph_parameters
  )
    :
    pdevs::GraphManager<common::DoubleTime>(coordinator, parameters, graph_parameters),
    S1("S1", artis::common::NoParameters(), artis::common::NoParameters()),
    S2("S2", artis::common::NoParameters(), artis::common::NoParameters()) {
    add_child(OneS1, &S1);
    add_child(OneS2, &S2);

    out({&S1, S1GraphManager::OUT}) >> in({&S2, S2GraphManager::IN});
  }

  ~Root3GraphManager() override = default;

private:
  artis::pdevs::Coordinator<common::DoubleTime, Linear2GraphManager> S1;
  artis::pdevs::Coordinator<common::DoubleTime, Linear2GraphManager> S2;
};

}
}
} // namespace artis tests mixed

#endif
