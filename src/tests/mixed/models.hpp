/**
 * @file tests/mixed/models.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTS_MIXED_MODELS_HPP
#define TESTS_MIXED_MODELS_HPP

#include <artis-star/common/time/DoubleTime.hpp>
#include <artis-star/common/utils/Trace.hpp>

#include <artis-star/kernel/dtss/Coordinator.hpp>
#include <artis-star/kernel/dtss/Dynamics.hpp>

#include <artis-star/kernel/pdevs/Dynamics.hpp>

namespace artis {
namespace tests {
namespace mixed {

class A1 :
    public artis::pdevs::Dynamics<common::DoubleTime, A1>
{
public:
  enum inputs
  {
    IN
  };
  enum outputs
  {
    OUT
  };

  A1(const std::string &name,
     const artis::pdevs::Context<common::DoubleTime, A1, artis::common::NoParameters> &context)
      :
      artis::pdevs::Dynamics<common::DoubleTime, A1>(name, context), _value(0)
  {
    input_ports({{IN, "in"}});
    output_ports({{OUT, "out"}});
  }

  ~A1() override = default;

  void dint(const typename common::DoubleTime::type &t) override
  {

#ifndef WITH_TRACE
    (void)t;
#endif

#ifdef WITH_TRACE
    common::Trace<common::DoubleTime>::trace()
        << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                    common::FormalismType::PDEVS,
                                                    common::FunctionType::DELTA_INT,
                                                    common::LevelType::USER);
    common::Trace<common::DoubleTime>::trace().flush();
#endif

    if (_phase == SEND or _phase == INIT) {
      _phase = WAIT;
    }
  }

  void
  dext(const typename common::DoubleTime::type &t,
       const typename common::DoubleTime::type & /* e */,
       const common::event::Bag<common::DoubleTime> &bag) override
  {

#ifndef WITH_TRACE
    (void)t;
    (void)bag;
#endif

#ifdef WITH_TRACE
    common::Trace<common::DoubleTime>::trace()
        << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                    common::FormalismType::PDEVS,
                                                    common::FunctionType::DELTA_EXT,
                                                    common::LevelType::USER)
        << "messages = " << bag.to_string();
    common::Trace<common::DoubleTime>::trace().flush();
#endif

    _phase = SEND;
  }

  void dconf(const typename common::DoubleTime::type &t,
             const typename common::DoubleTime::type & /* e */,
             const common::event::Bag<common::DoubleTime> &bag) override
  {

#ifndef WITH_TRACE
    (void)t;
    (void)bag;
#endif

#ifdef WITH_TRACE
    common::Trace<common::DoubleTime>::trace()
        << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                    common::FormalismType::PDEVS,
                                                    common::FunctionType::DELTA_CONF,
                                                    common::LevelType::USER)
        << "messages = " << bag.to_string();
    common::Trace<common::DoubleTime>::trace().flush();
#endif
  }

  void start(const typename common::DoubleTime::type &t) override
  {

#ifndef WITH_TRACE
    (void)t;
#endif

#ifdef WITH_TRACE
    common::Trace<common::DoubleTime>::trace()
        << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                    common::FormalismType::PDEVS,
                                                    common::FunctionType::START,
                                                    common::LevelType::USER);
    common::Trace<common::DoubleTime>::trace().flush();
#endif
    _phase = INIT;
  }

  typename common::DoubleTime::type
  ta(const typename common::DoubleTime::type &t) const override
  {
#ifndef WITH_TRACE
    (void)t;
#endif

#ifdef WITH_TRACE
    common::Trace<common::DoubleTime>::trace()
        << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                    common::FormalismType::PDEVS,
                                                    common::FunctionType::TA,
                                                    common::LevelType::USER);
    common::Trace<common::DoubleTime>::trace().flush();
#endif
    if (_phase == WAIT) {
      return 3;
    } else {
      return 0;
    }
  }

  common::event::Bag<common::DoubleTime>
  lambda(const typename common::DoubleTime::type &t) const override
  {

#ifndef WITH_TRACE
    (void)t;
#endif

    common::event::Bag<common::DoubleTime> bag;

    bag.push_back(artis::common::event::ExternalEvent<common::DoubleTime>(OUT, _value));

#ifdef WITH_TRACE
    common::Trace<common::DoubleTime>::trace()
        << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                    common::FormalismType::PDEVS,
                                                    common::FunctionType::LAMBDA,
                                                    common::LevelType::USER)
        << "messages = " << bag.to_string();
    common::Trace<common::DoubleTime>::trace().flush();
#endif

    return bag;
  }

private:
  enum Phase
  {
    INIT, WAIT, SEND
  };

  Phase _phase;
  double _value;
};

class B1 :
    public artis::pdevs::Dynamics<common::DoubleTime, B1>
{
public:
  enum inputs
  {
    IN
  };
  enum outputs
  {
    OUT
  };

  B1(const std::string &name,
     const artis::pdevs::Context<common::DoubleTime, B1, artis::common::NoParameters> &context)
      :
      artis::pdevs::Dynamics<common::DoubleTime, B1>(name, context)
  {
    input_ports({{IN, "in"}});
    output_ports({{OUT, "out"}});
  }

  ~B1() override = default;

  void dint(const typename common::DoubleTime::type &t) override
  {

#ifndef WITH_TRACE
    (void)t;
#endif

#ifdef WITH_TRACE
    common::Trace<common::DoubleTime>::trace()
        << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                    common::FormalismType::PDEVS,
                                                    common::FunctionType::DELTA_INT,
                                                    common::LevelType::USER);
    common::Trace<common::DoubleTime>::trace().flush();
#endif

    if (_phase == SEND or _phase == INIT) {
      _phase = WAIT;
    }
  }

  void
  dext(const typename common::DoubleTime::type &t,
       const typename common::DoubleTime::type &e,
       const common::event::Bag<common::DoubleTime> &bag) override
  {

#ifdef WITH_TRACE
    common::Trace<common::DoubleTime>::trace()
        << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                    common::FormalismType::PDEVS,
                                                    common::FunctionType::DELTA_EXT,
                                                    common::LevelType::USER)
        << "messages = " << bag.to_string();
    common::Trace<common::DoubleTime>::trace().flush();
#endif

    std::for_each(bag.begin(), bag.end(),
                  [this, t, e](
                      const common::event::ExternalEvent<common::DoubleTime> & /* event */) {
                    ++_count;
                  });
    _phase = SEND;
  }

  void dconf(const typename common::DoubleTime::type &t,
             const typename common::DoubleTime::type &e,
             const common::event::Bag<common::DoubleTime> &bag) override
  {

#ifndef WITH_TRACE
    (void)t;
    (void)bag;
#endif

#ifdef WITH_TRACE
    common::Trace<common::DoubleTime>::trace()
        << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                    common::FormalismType::PDEVS,
                                                    common::FunctionType::DELTA_CONF,
                                                    common::LevelType::USER)
        << "messages = " << bag.to_string();
    common::Trace<common::DoubleTime>::trace().flush();
#endif

    dext(t, e, bag);
    dint(t);

  }

  void start(const typename common::DoubleTime::type &t) override
  {

#ifndef WITH_TRACE
    (void)t;
#endif

#ifdef WITH_TRACE
    common::Trace<common::DoubleTime>::trace()
        << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                    common::FormalismType::PDEVS,
                                                    common::FunctionType::START,
                                                    common::LevelType::USER);
    common::Trace<common::DoubleTime>::trace().flush();
#endif

    _count = 0;
    _phase = INIT;
  }

  typename common::DoubleTime::type
  ta(const typename common::DoubleTime::type &t) const override
  {

#ifndef WITH_TRACE
    (void)t;
#endif

#ifdef WITH_TRACE
    common::Trace<common::DoubleTime>::trace()
        << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                    common::FormalismType::PDEVS,
                                                    common::FunctionType::TA,
                                                    common::LevelType::USER);
    common::Trace<common::DoubleTime>::trace().flush();
#endif

    if (_phase == WAIT) {
      return std::numeric_limits<double>::max();
    } else {
      return 0;
    }
  }

  common::event::Bag<common::DoubleTime>
  lambda(const typename common::DoubleTime::type &t) const override
  {

#ifndef WITH_TRACE
    (void)t;
#endif
    common::event::Bag<common::DoubleTime> bag;

    bag.push_back(artis::common::event::ExternalEvent<common::DoubleTime>(OUT, _count));

#ifdef WITH_TRACE
    common::Trace<common::DoubleTime>::trace()
        << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                    common::FormalismType::PDEVS,
                                                    common::FunctionType::LAMBDA,
                                                    common::LevelType::USER)
        << "messages = " << bag.to_string();
    common::Trace<common::DoubleTime>::trace().flush();
#endif

    return bag;
  }

private:
  enum Phase
  {
    INIT, WAIT, SEND
  };

  Phase _phase;
  unsigned int _count;
};

class A2
    : public artis::dtss::Dynamics<artis::common::DoubleTime,
                                   A2,
                                   artis::dtss::Parameters<common::DoubleTime>>
{
public:
  enum inputs
  {
    IN
  };
  enum outputs
  {
    OUT
  };

  A2(const std::string &name,
     const artis::dtss::Context<artis::common::DoubleTime,
                                A2,
                                artis::dtss::Parameters<common::DoubleTime>> &context)
      :
      artis::dtss::Dynamics<artis::common::DoubleTime,
                            A2,
                            artis::dtss::Parameters<common::DoubleTime>>(
          name, context),
      _value(0)
  {
    input_ports({{IN, "in"}});
    output_ports({{OUT, "out"}});
  }

  ~A2() override = default;

  void transition(const common::event::Bag<common::DoubleTime> &x,
                  const typename common::DoubleTime::type &t) override
  {

#ifndef WITH_TRACE
    (void)x;
    (void)t;
#endif

#ifdef WITH_TRACE
    common::Trace<common::DoubleTime>::trace()
        << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                    common::FormalismType::DTSS,
                                                    common::FunctionType::TRANSITION,
                                                    common::LevelType::USER)
        << "x = " << x.to_string();
    common::Trace<common::DoubleTime>::trace().flush();
#endif

  }

  void start(const typename common::DoubleTime::type &t) override
  {

#ifndef WITH_TRACE
    (void)t;
#endif

#ifdef WITH_TRACE
    common::Trace<common::DoubleTime>::trace()
        << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                    common::FormalismType::DTSS,
                                                    common::FunctionType::START,
                                                    common::LevelType::USER);
    common::Trace<common::DoubleTime>::trace().flush();
#endif
  }

  common::event::Bag<common::DoubleTime>
  lambda(const typename common::DoubleTime::type &t) const override
  {

#ifndef WITH_TRACE
    (void)t;
#endif
    common::event::Bag<common::DoubleTime> bag;

    bag.push_back(artis::common::event::ExternalEvent<common::DoubleTime>(OUT, _value));

#ifdef WITH_TRACE
    common::Trace<common::DoubleTime>::trace()
        << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                    common::FormalismType::DTSS,
                                                    common::FunctionType::LAMBDA,
                                                    common::LevelType::USER)
        << "messages = " << bag.to_string();
    common::Trace<common::DoubleTime>::trace().flush();
#endif

    return bag;
  }

private:
  double _value;
};

class B2
    : public artis::dtss::Dynamics<artis::common::DoubleTime,
                                   B2,
                                   artis::dtss::Parameters<common::DoubleTime>>
{
public:
  enum inputs
  {
    IN
  };
  enum outputs
  {
    OUT
  };

  B2(const std::string &name,
     const artis::dtss::Context<artis::common::DoubleTime,
                                B2,
                                artis::dtss::Parameters<common::DoubleTime>> &context)
      :
      artis::dtss::Dynamics<artis::common::DoubleTime,
                            B2,
                            artis::dtss::Parameters<common::DoubleTime>>(
          name, context),
      _value(0)
  {
    input_ports({{IN, "in"}});
    output_ports({{OUT, "out"}});
  }

  ~B2() override = default;

  void transition(const common::event::Bag<common::DoubleTime> &x,
                  const typename common::DoubleTime::type &t) override
  {

#ifndef WITH_TRACE
    (void)x;
    (void)t;
#endif

#ifdef WITH_TRACE
    common::Trace<common::DoubleTime>::trace()
        << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                    common::FormalismType::DTSS,
                                                    common::FunctionType::TRANSITION,
                                                    common::LevelType::USER)
        << "x = " << x.to_string();
    common::Trace<common::DoubleTime>::trace().flush();
#endif

  }

  void start(const typename common::DoubleTime::type &t) override
  {

#ifndef WITH_TRACE
    (void)t;
#endif

#ifdef WITH_TRACE
    common::Trace<common::DoubleTime>::trace()
        << common::TraceElement<common::DoubleTime>(
            get_name(), t,
            common::FormalismType::DTSS,
            common::FunctionType::START,
            common::LevelType::USER);
    common::Trace<common::DoubleTime>::trace().flush();
#endif

  }

  common::event::Bag<common::DoubleTime>
  lambda(const typename common::DoubleTime::type &t) const override
  {

#ifndef WITH_TRACE
    (void)t;
#endif
    common::event::Bag<common::DoubleTime> bag;

    bag.push_back(artis::common::event::ExternalEvent<common::DoubleTime>(OUT, _value));

#ifdef WITH_TRACE
    common::Trace<common::DoubleTime>::trace()
        << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                    common::FormalismType::DTSS,
                                                    common::FunctionType::LAMBDA,
                                                    common::LevelType::USER)
        << "messages = " << bag.to_string();
    common::Trace<common::DoubleTime>::trace().flush();
#endif

    return bag;
  }

private:
  double _value;
};

class Beep :
    public artis::pdevs::Dynamics<common::DoubleTime, Beep>
{
public:
  enum inputs
  {
    IN
  };
  enum outputs
  {
    OUT
  };

  Beep(const std::string &name,
       const artis::pdevs::Context<common::DoubleTime, Beep, artis::common::NoParameters> &context)
      :
      artis::pdevs::Dynamics<common::DoubleTime, Beep>(name, context), _value(0)
  {
    input_ports({{IN, "in"}});
    output_ports({{OUT, "out"}});
  }

  ~Beep() override = default;

  void dint(const typename common::DoubleTime::type &t) override
  {

#ifndef WITH_TRACE
    (void)t;
#endif

#ifdef WITH_TRACE
    common::Trace<common::DoubleTime>::trace()
        << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                    common::FormalismType::PDEVS,
                                                    common::FunctionType::DELTA_INT,
                                                    common::LevelType::USER);
    common::Trace<common::DoubleTime>::trace().flush();
#endif

    if (_phase == SEND or _phase == INIT) {
      _phase = WAIT;
    }
  }

  void
  dext(const typename common::DoubleTime::type &t,
       const typename common::DoubleTime::type & /* e */,
       const common::event::Bag<common::DoubleTime> &bag) override
  {

#ifndef WITH_TRACE
    (void)t;
    (void)bag;
#endif

#ifdef WITH_TRACE
    common::Trace<common::DoubleTime>::trace()
        << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                    common::FormalismType::PDEVS,
                                                    common::FunctionType::DELTA_EXT,
                                                    common::LevelType::USER)
        << "messages = " << bag.to_string();
    common::Trace<common::DoubleTime>::trace().flush();
#endif

    _phase = SEND;
  }

  void dconf(const typename common::DoubleTime::type &t,
             const typename common::DoubleTime::type & /* e */,
             const common::event::Bag<common::DoubleTime> &bag) override
  {

#ifndef WITH_TRACE
    (void)t;
    (void)bag;
#endif

#ifdef WITH_TRACE
    common::Trace<common::DoubleTime>::trace()
        << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                    common::FormalismType::PDEVS,
                                                    common::FunctionType::DELTA_CONF,
                                                    common::LevelType::USER)
        << "messages = " << bag.to_string();
    common::Trace<common::DoubleTime>::trace().flush();
#endif

  }

  void start(const typename common::DoubleTime::type &t) override
  {

#ifndef WITH_TRACE
    (void)t;
#endif

#ifdef WITH_TRACE
    common::Trace<common::DoubleTime>::trace()
        << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                    common::FormalismType::PDEVS,
                                                    common::FunctionType::START,
                                                    common::LevelType::USER);
    common::Trace<common::DoubleTime>::trace().flush();
#endif

    _phase = INIT;
  }

  typename common::DoubleTime::type
  ta(const typename common::DoubleTime::type &t) const override
  {

#ifndef WITH_TRACE
    (void)t;
#endif

#ifdef WITH_TRACE
    common::Trace<common::DoubleTime>::trace()
        << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                    common::FormalismType::PDEVS,
                                                    common::FunctionType::TA,
                                                    common::LevelType::USER);
    common::Trace<common::DoubleTime>::trace().flush();
#endif

    if (_phase == WAIT) {
      return (rand() % 100) / 10.;
    } else {
      return 0;
    }
  }

  common::event::Bag<common::DoubleTime>
  lambda(const typename common::DoubleTime::type &t) const override
  {

#ifndef WITH_TRACE
    (void)t;
#endif
    common::event::Bag<common::DoubleTime> bag;

    bag.push_back(artis::common::event::ExternalEvent<common::DoubleTime>(OUT, _value));

#ifdef WITH_TRACE
    common::Trace<common::DoubleTime>::trace()
        << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                    common::FormalismType::PDEVS,
                                                    common::FunctionType::LAMBDA,
                                                    common::LevelType::USER)
        << "messages = " << bag.to_string();
    common::Trace<common::DoubleTime>::trace().flush();
#endif

    return bag;
  }

private:
  enum Phase
  {
    INIT, WAIT, SEND
  };

  Phase _phase;
  double _value;
};

}
}
} // namespace artis tests mixed

#endif
