/**
 * @file tests/qss/graph_manager.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTS_QSS_GRAPH_MANAGER_HPP
#define TESTS_QSS_GRAPH_MANAGER_HPP

#include <tests/qss/models.hpp>

#include <artis-star/kernel/pdevs/Coordinator.hpp>
#include <artis-star-addons/qss/GraphManager.hpp>
#include <artis-star-addons/qss/MultiGraphManager.hpp>
#include <artis-star/kernel/dtss/Coordinator.hpp>
#include <artis-star/kernel/dtss/GraphManager.hpp>
#include <artis-star/kernel/dtss/Policy.hpp>

namespace artis::tests::qss {

class ConstantGraphManager :
  public artis::addons::qss::GraphManager<common::DoubleTime, Constant> {
public:
  ConstantGraphManager(common::Coordinator<common::DoubleTime> *coordinator,
                       const artis::addons::qss::QSSParameters<artis::common::NoParameters> &parameters,
                       const artis::common::NoParameters &graph_parameters)
    :
    artis::addons::qss::GraphManager<common::DoubleTime, Constant>(coordinator,
                                                                   parameters, graph_parameters) {}

  ~ConstantGraphManager() override = default;
};

class OnlyOneGraphManager :
  public artis::addons::qss::GraphManager<common::DoubleTime, Constant> {
public:
  enum submodels {
    A
  };

  OnlyOneGraphManager(common::Coordinator<common::DoubleTime> *coordinator,
                      const artis::addons::qss::QSSParameters<artis::common::NoParameters> &parameters,
                      const artis::common::NoParameters &graph_parameters)
    :
    artis::addons::qss::GraphManager<common::DoubleTime, Constant>(coordinator,
                                                                   parameters, graph_parameters),
    S("a", parameters, graph_parameters) {
    add_child(A, &S);
  }

  ~OnlyOneGraphManager() override = default;

private:
  artis::pdevs::Coordinator<common::DoubleTime,
    ConstantGraphManager,
    artis::addons::qss::QSSParameters<artis::common::NoParameters>> S;
};

class ParabolaGraphManager :
  public artis::addons::qss::GraphManager<common::DoubleTime, Parabola, ParabolaParameters> {
public:
  ParabolaGraphManager(common::Coordinator<common::DoubleTime> *coordinator,
                       const artis::addons::qss::QSSParameters<ParabolaParameters> &parameters,
                       const artis::common::NoParameters &graph_parameters)
    :
    artis::addons::qss::GraphManager<common::DoubleTime, Parabola, ParabolaParameters>(
      coordinator, parameters, graph_parameters) {}

  ~ParabolaGraphManager() override = default;
};

class OnlyOneParabolaGraphManager :
  public artis::pdevs::GraphManager<common::DoubleTime,
    artis::addons::qss::QSSParameters<ParabolaParameters>> {
public:
  enum submodels {
    A
  };

  OnlyOneParabolaGraphManager(common::Coordinator<common::DoubleTime> *coordinator,
                              const artis::addons::qss::QSSParameters<ParabolaParameters> &parameters,
                              const artis::common::NoParameters &graph_parameters)
    :
    artis::pdevs::GraphManager<common::DoubleTime, artis::addons::qss::QSSParameters<ParabolaParameters>>(
      coordinator, parameters, graph_parameters),
    S("a", parameters, graph_parameters) {
    add_child(A, &S);
  }

  ~OnlyOneParabolaGraphManager() override = default;

private:
  artis::pdevs::Coordinator<common::DoubleTime,
    ParabolaGraphManager,
    artis::addons::qss::QSSParameters<ParabolaParameters>> S;
};

class PredatorGraphManager :
  public artis::addons::qss::GraphManager<common::DoubleTime, Predator, PreyPredatorParameters> {
public:
  enum inputs {
    IN_X =
    artis::addons::qss::GraphManager<common::DoubleTime, Predator, PreyPredatorParameters>::input::RESET + 1
  };

  PredatorGraphManager(common::Coordinator<common::DoubleTime> *coordinator,
                       const artis::addons::qss::QSSParameters<PreyPredatorParameters> &parameters,
                       const artis::common::NoParameters &graph_parameters)
    :
    artis::addons::qss::GraphManager<common::DoubleTime, Predator, PreyPredatorParameters>(
      coordinator, parameters, graph_parameters) {
    coordinator->input_port({IN_X, "in_x"});
    in({coordinator, IN_X}) >> in({derivative(), derivative()->dynamics().IN_X});
  }

  ~PredatorGraphManager() override = default;
};

class PreyGraphManager :
  public artis::addons::qss::GraphManager<common::DoubleTime, Prey, PreyPredatorParameters> {
public:
  enum inputs {
    IN_Y =
    artis::addons::qss::GraphManager<common::DoubleTime, Prey, PreyPredatorParameters>::input::RESET
    + 1
  };

  PreyGraphManager(common::Coordinator<common::DoubleTime> *coordinator,
                   const artis::addons::qss::QSSParameters<PreyPredatorParameters> &parameters,
                   const artis::common::NoParameters &graph_parameters)
    :
    artis::addons::qss::GraphManager<common::DoubleTime, Prey, PreyPredatorParameters>(
      coordinator, parameters, graph_parameters) {
    coordinator->input_port({IN_Y, "in_y"});
    in({coordinator, IN_Y}) >> in({derivative(), derivative()->dynamics().IN_Y});
  }

  ~PreyGraphManager() override = default;
};

struct PreyPredatorGraphManagerParameters {
  artis::addons::qss::QSSParameters<PreyPredatorParameters> _predator;
  artis::addons::qss::QSSParameters<PreyPredatorParameters> _prey;
};

class PreyPredatorGraphManager :
  public artis::pdevs::GraphManager<common::DoubleTime, PreyPredatorGraphManagerParameters> {
public:
  enum submodels {
    PREDATOR, PREY
  };

  enum inputs {
    RESET_X, RESET_Y
  };

  enum outputs {
    OUT_X, OUT_Y
  };

  PreyPredatorGraphManager(common::Coordinator<common::DoubleTime> *coordinator,
                           const PreyPredatorGraphManagerParameters &parameters,
                           const artis::common::NoParameters &graph_parameters)
    :
    artis::pdevs::GraphManager<common::DoubleTime, PreyPredatorGraphManagerParameters>(
      coordinator, parameters, graph_parameters),
    _predator("predator", parameters._predator, graph_parameters),
    _prey("prey", parameters._prey, graph_parameters) {
    add_child(PREDATOR, &_predator);
    add_child(PREY, &_prey);

    coordinator->input_ports({{RESET_X, "reset_x"},
                              {RESET_Y, "reset_y"}});
    coordinator->output_ports({{OUT_X, "out_x"},
                               {OUT_Y, "out_y"}});

    in({coordinator, RESET_X}) >> in({&_prey, PreyGraphManager::input::RESET});
    in({coordinator, RESET_Y}) >> in({&_prey, PreyGraphManager::IN_Y});

    in({coordinator, RESET_X}) >> in({&_predator, PredatorGraphManager::IN_X});
    in({coordinator, RESET_Y}) >> in({&_predator, PredatorGraphManager::input::RESET});

    out({&_prey, PreyGraphManager::output::OUT}) >> out({coordinator, OUT_X});
    out({&_predator, PredatorGraphManager::output::OUT}) >> out({coordinator, OUT_Y});

    out({&_predator, PredatorGraphManager::output::OUT})
      >> in({&_prey, PreyGraphManager::IN_Y});
    out({&_prey, PreyGraphManager::output::OUT})
      >> in({&_predator, PredatorGraphManager::IN_X});
  }

  ~PreyPredatorGraphManager() override = default;

private:
  artis::pdevs::Coordinator<common::DoubleTime,
    PredatorGraphManager,
    artis::addons::qss::QSSParameters<PreyPredatorParameters>> _predator;
  artis::pdevs::Coordinator<common::DoubleTime,
    PreyGraphManager,
    artis::addons::qss::QSSParameters<PreyPredatorParameters>> _prey;
};

struct PreyPredatorSmartGardenerGraphManagerParameters {
  PreyPredatorGraphManagerParameters _prey_predator;
  SmartGardenerParameters _smart_gardener;
};

class PreyPredatorSmartGardenerGraphManager :
  public artis::pdevs::GraphManager<common::DoubleTime,
    PreyPredatorSmartGardenerGraphManagerParameters> {
public:
  enum submodels {
    PREY_PREDATOR, SMART_GARDENER
  };

  PreyPredatorSmartGardenerGraphManager(
    common::Coordinator<common::DoubleTime> *coordinator,
    const PreyPredatorSmartGardenerGraphManagerParameters &parameters,
    const artis::common::NoParameters &graph_parameters)
    :
    artis::pdevs::GraphManager<common::DoubleTime,
      PreyPredatorSmartGardenerGraphManagerParameters>(
      coordinator, parameters, graph_parameters),
    _prey_predator("prey_predator", parameters._prey_predator,
                   graph_parameters),
    _smart_gardener("smart_gardener", parameters._smart_gardener) {
    add_child(PREY_PREDATOR, &_prey_predator);
    add_child(SMART_GARDENER, &_smart_gardener);

    out({&_prey_predator, PreyPredatorGraphManager::OUT_X})
      >> in({&_smart_gardener, SmartGardener::IN_X});
    out({&_prey_predator, PreyPredatorGraphManager::OUT_Y})
      >> in({&_smart_gardener, SmartGardener::IN_Y});

    out({&_smart_gardener, SmartGardener::OUT_X})
      >> in({&_prey_predator, PreyPredatorGraphManager::RESET_X});
    out({&_smart_gardener, SmartGardener::OUT_Y})
      >> in({&_prey_predator, PreyPredatorGraphManager::RESET_Y});
  }

  ~PreyPredatorSmartGardenerGraphManager() override = default;

private:
  artis::pdevs::Coordinator<common::DoubleTime,
    PreyPredatorGraphManager,
    PreyPredatorGraphManagerParameters> _prey_predator;
  artis::pdevs::Simulator<common::DoubleTime, SmartGardener, SmartGardenerParameters>
    _smart_gardener;
};

class DiscretePredatorGraphManager :
  public artis::dtss::GraphManager<artis::common::DoubleTime, DiscretePreyPredatorParameters> {
public:
  enum submodels {
    PREDATOR
  };
  enum inputs {
    RESET, IN_X
  };
  enum outputs {
    OUT
  };

  DiscretePredatorGraphManager(
    artis::common::Coordinator<artis::common::DoubleTime> *coordinator,
    const DiscretePreyPredatorParameters &parameters,
    const artis::common::NoParameters &graph_parameters)
    :
    artis::dtss::GraphManager<artis::common::DoubleTime, DiscretePreyPredatorParameters>(coordinator, parameters,
                                                                                         graph_parameters),
    _predator("predator", parameters, parameters.time_step) {
    add_child(PREDATOR, &_predator);

    coordinator->input_ports({{RESET, "reset"},
                              {IN_X,  "in_x"}});
    coordinator->output_port({OUT, "out"});

    out({&_predator, DiscretePredator::OUT}) >> out({coordinator, OUT});
    in({coordinator, RESET}) >> in({&_predator, DiscretePredator::RESET});
    in({coordinator, IN_X}) >> in({&_predator, DiscretePredator::IN_X});
  }

  ~DiscretePredatorGraphManager() override = default;

private:
  artis::dtss::Simulator<artis::common::DoubleTime, DiscretePredator, DiscretePreyPredatorParameters> _predator;
};

struct MixedPreyPredatorGraphManagerParameters {
  DiscretePreyPredatorParameters _predator;
  artis::addons::qss::QSSParameters<PreyPredatorParameters> _prey;
};

class MixedPreyPredatorGraphManager :
  public artis::pdevs::GraphManager<artis::common::DoubleTime,
    MixedPreyPredatorGraphManagerParameters> {
public:
  enum submodels {
    PREDATOR, PREY
  };

  enum inputs {
    RESET_X, RESET_Y
  };

  enum outputs {
    OUT_X, OUT_Y
  };

  MixedPreyPredatorGraphManager(
    artis::common::Coordinator<artis::common::DoubleTime> *coordinator,
    const MixedPreyPredatorGraphManagerParameters &parameters,
    const artis::common::NoParameters &graph_parameters)
    :
    artis::pdevs::GraphManager<artis::common::DoubleTime,
      MixedPreyPredatorGraphManagerParameters>(
      coordinator, parameters, graph_parameters),
    _predator("predator", parameters._predator, graph_parameters),
    _prey("prey", parameters._prey, graph_parameters) {
    add_child(PREDATOR, &_predator);
    add_child(PREY, &_prey);

    coordinator->input_ports({{RESET_X, "reset_x"},
                              {RESET_Y, "reset_y"}});
    coordinator->output_ports({{OUT_X, "out_x"},
                               {OUT_Y, "out_y"}});

    in({coordinator, RESET_X}) >> in({&_prey, PreyGraphManager::input::RESET});
    in({coordinator, RESET_Y}) >> in({&_predator, PredatorGraphManager::input::RESET});

    out({&_prey, PreyGraphManager::output::OUT}) >> out({coordinator, OUT_X});
    out({&_predator, PredatorGraphManager::output::OUT}) >> out({coordinator, OUT_Y});

    out({&_predator, PredatorGraphManager::output::OUT})
      >> in({&_prey, PreyGraphManager::IN_Y});
    out({&_prey, PreyGraphManager::output::OUT})
      >> in({&_predator, PredatorGraphManager::IN_X});
  }

  ~MixedPreyPredatorGraphManager() override = default;

private:
  artis::dtss::Coordinator<artis::common::DoubleTime,
    artis::dtss::LastBagPolicy,
    DiscretePredatorGraphManager,
    DiscretePreyPredatorParameters> _predator;
  artis::pdevs::Coordinator<artis::common::DoubleTime,
    PreyGraphManager,
    artis::addons::qss::QSSParameters<PreyPredatorParameters>> _prey;
};

class MultiPreyPredatorGraphManager :
  public artis::addons::qss::MultiGraphManager<common::DoubleTime,
    PreyPredator,
    PreyPredatorParameters> {
public:
  MultiPreyPredatorGraphManager(common::Coordinator<common::DoubleTime> *coordinator,
                                const artis::addons::qss::MultiQSSParameters<
                                  PreyPredatorParameters> &parameters,
                                const artis::common::NoParameters &graph_parameters)
    :
    artis::addons::qss::MultiGraphManager<common::DoubleTime,
      PreyPredator,
      PreyPredatorParameters>(
      coordinator, parameters, graph_parameters) {}

  ~MultiPreyPredatorGraphManager() override = default;
};

}

// namespace artis tests qss

#endif
