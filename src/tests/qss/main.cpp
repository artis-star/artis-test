/**
 * @file pdevs/tests.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <tests/qss/graph_manager.hpp>

#include <artis-star/common/RootCoordinator.hpp>
#include <artis-star/common/observer/TimedIterator.hpp>
#include <artis-star/common/observer/Output.hpp>

#include <fstream>
#include <iostream>
#include <boost/archive/binary_oarchive.hpp>

using namespace artis::tests::qss;

void test_parabola() {
  artis::addons::qss::QSSParameters<ParabolaParameters> parameters = {{0.5},
                                                              {true, true, 0.001, 3},
                                                              {0.2}};

  class View : public artis::common::observer::View<artis::common::DoubleTime> {
  public:
    View() {
      selector("Value",
               {OnlyOneParabolaGraphManager::A, ParabolaGraphManager::submodel::S_Integrator,
                artis::qss::Integrator<artis::common::DoubleTime>::var::VALUE});
    }
  };

  artis::common::context::Context<artis::common::DoubleTime> context(0, 5);
  artis::common::RootCoordinator<
    artis::common::DoubleTime, artis::pdevs::Coordinator<
      artis::common::DoubleTime,
      OnlyOneParabolaGraphManager,
      artis::addons::qss::QSSParameters<ParabolaParameters>>
  > rc(context, "root", parameters, artis::common::NoParameters());

//  rc.attachView("Value", new View());

  rc.run(context);

//  const View::Values &values = rc.observer().view("Value").get("Value");
//
//  for (const auto &value: values) {
//    double v;
//
//    value.second(v);
//    std::cout << value.first << ": " << v << std::endl;
//  }
}

class PredatorPreyView : public artis::common::observer::View<artis::common::DoubleTime> {
public:
  PredatorPreyView() {
    selector("PredatorView",
             {PreyPredatorGraphManager::PREDATOR, PredatorGraphManager::submodel::S_Integrator,
              artis::qss::Integrator<artis::common::DoubleTime>::var::VALUE});
    selector("PreyView",
             {PreyPredatorGraphManager::PREY, PreyGraphManager::submodel::S_Integrator,
              artis::qss::Integrator<artis::common::DoubleTime>::var::VALUE});
  }
};

void run_predator_prey(artis::common::context::Context<artis::common::DoubleTime> &context,
                       const PreyPredatorGraphManagerParameters &parameters) {
  artis::common::RootCoordinator<
    artis::common::DoubleTime, artis::pdevs::Coordinator<
      artis::common::DoubleTime,
      PreyPredatorGraphManager,
      PreyPredatorGraphManagerParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

//  rc.attachView("Value1", new PredatorPreyView());

  rc.run(context);

//  artis::common::observer::Output<artis::common::DoubleTime,
//    artis::common::observer::TimedIterator<artis::common::DoubleTime>>
//    output(rc.observer());
//
//  output(context.begin(), context.end(), {context.begin(), 0.1});

  //  rc.save(context);
//
//  std::ofstream os("state");
//  boost::archive::binary_oarchive oa(os);
//
//  oa << context;
//
//  {
//    artis::observer::DiscreteTimeIterator<artis::common::DoubleTime> it(
//        rc.observer().view("Value").get("PredatorView"), context.begin(), 0.1);
//
//    while (it.has_next()) {
//      double v;
//
//      (*it).second(v);
//      std::cout << (*it).first << ";" << v << std::endl;
//      ++it;
//    }
//  }
//  {
//    artis::observer::DiscreteTimeIterator<artis::common::DoubleTime> it(
//        rc.observer().view("Value").get("PreyView"), context.begin(), 0.1);
//
//    while (it.has_next()) {
//      double v;
//
//      (*it).second(v);
//      std::cout << (*it).first << ";" << v << std::endl;
//      ++it;
//    }
//  }
}

void test_predator_prey() {
  PreyPredatorGraphManagerParameters parameters = {{{45.},
                                                     {true, true, 0.1, 3},
                                                     {0.5, 0.01, 0.01, 0.2}},
                                                   {{5000.},
                                                     {true, true, 1,   3},
                                                     {0.5, 0.01, 0.01, 0.2}}};

  artis::common::context::Context<artis::common::DoubleTime> context(0, 100);

  run_predator_prey(context, parameters);

//  artis::common::context::Context<artis::common::DoubleTime> new_context(context);
//
//  new_context.end(200);
//  run_predator_prey(new_context, parameters);
}

class PredatorPreySmartGardenerView : public artis::common::observer::View<artis::common::DoubleTime> {
public:
  PredatorPreySmartGardenerView() {
    selector("PredatorView",
             {PreyPredatorSmartGardenerGraphManager::PREY_PREDATOR,
              PreyPredatorGraphManager::PREDATOR, PredatorGraphManager::submodel::S_Integrator,
              artis::qss::Integrator<artis::common::DoubleTime>::var::VALUE});
    selector("PreyView",
             {PreyPredatorSmartGardenerGraphManager::PREY_PREDATOR,
              PreyPredatorGraphManager::PREY, PreyGraphManager::submodel::S_Integrator,
              artis::qss::Integrator<artis::common::DoubleTime>::var::VALUE});
  }
};

void test_predator_prey_smart_gardener() {
  PreyPredatorSmartGardenerGraphManagerParameters parameters = {{{{45.},
                                                                   {true, true, 0.1, 3},
                                                                   {0.5, 0.01, 0.01, 0.2}},
                                                                       {{5000.},
                                                                         {true, true, 1, 3},
                                                                         {0.5, 0.01, 0.01, 0.2}}},
                                                                {2000, 0.25, 0.75, 5}};

  artis::common::context::Context<artis::common::DoubleTime> context(0, 100);
  artis::common::RootCoordinator<
    artis::common::DoubleTime, artis::pdevs::Coordinator<
      artis::common::DoubleTime,
      PreyPredatorSmartGardenerGraphManager,
      PreyPredatorSmartGardenerGraphManagerParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

//  rc.attachView("Value2", new PredatorPreySmartGardenerView());

  rc.run(context);

//  artis::common::observer::Output<artis::common::DoubleTime,
//    artis::common::observer::TimedIterator<artis::common::DoubleTime>>
//    output(rc.observer());
//
//  output(context.begin(), context.end(), {context.begin(), 0.1});
}

class MixedPredatorPreyView : public artis::common::observer::View<artis::common::DoubleTime> {
public:
  MixedPredatorPreyView() {
    selector("PredatorView",
             {MixedPreyPredatorGraphManager::PREDATOR, DiscretePredatorGraphManager::PREDATOR,
              DiscretePredator::VALUE});
    selector("PreyView",
             {MixedPreyPredatorGraphManager::PREY, PreyGraphManager::submodel::S_Integrator,
              artis::qss::Integrator<artis::common::DoubleTime>::var::VALUE});
  }
};

void test_mixed_predator_prey() {
  MixedPreyPredatorGraphManagerParameters parameters = {{0.0001, 45., 0.5, 0.01, 0.01, 0.2},
                                                        {{5000.},
                                                                 {true, true, 1, 3},
                                                                      {0.5, 0.01, 0.01, 0.2}}};
  artis::common::context::Context<artis::common::DoubleTime> context(0, 100);
  artis::common::RootCoordinator<
    artis::common::DoubleTime, artis::pdevs::Coordinator<
      artis::common::DoubleTime,
      MixedPreyPredatorGraphManager,
      MixedPreyPredatorGraphManagerParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

//  rc.attachView("Value3", new MixedPredatorPreyView());

  rc.run(context);

//  artis::common::observer::Output<artis::common::DoubleTime,
//    artis::common::observer::TimedIterator<artis::common::DoubleTime>>
//    output(rc.observer());
//
//  output(context.begin(), context.end(), {context.begin(), 0.1});
}

class MultiPredatorPreyView : public artis::common::observer::View<artis::common::DoubleTime> {
public:
  MultiPredatorPreyView() {
    selector("PredatorView",
             {MultiPreyPredatorGraphManager::submodel::S_Integrator,
              artis::qss::Integrator<artis::common::DoubleTime>::var::VALUE});
    selector("PreyView",
             {MultiPreyPredatorGraphManager::submodel::S_Integrator + 1,
              artis::qss::Integrator<artis::common::DoubleTime>::var::VALUE});
    selector("PredatorDerivativeView",
             {MultiPreyPredatorGraphManager::submodel::S_Derivative,
              PreyPredator::var::VALUE});
    selector("PreyDerivativeView",
             {MultiPreyPredatorGraphManager::submodel::S_Derivative,
              PreyPredator::var::VALUE + 1});
  }
};

void test_multi_predator_prey() {
  artis::addons::qss::MultiQSSParameters<PreyPredatorParameters> parameters = {
    {{5000.},            {45.}},
    {{true, true, 1, 3}, {true, true, 0.1, 3}},
    {0.5,                0.01, 0.01, 0.2}
  };
  artis::common::context::Context<artis::common::DoubleTime> context(0, 100);
  artis::common::RootCoordinator<
    artis::common::DoubleTime, artis::pdevs::Coordinator<
      artis::common::DoubleTime,
      MultiPreyPredatorGraphManager,
      artis::addons::qss::MultiQSSParameters<PreyPredatorParameters>>
  > rc(context, "root", parameters, artis::common::NoParameters());

//  rc.attachView("Value4", new MultiPredatorPreyView());

  rc.run(context);

//  artis::common::observer::Output<artis::common::DoubleTime,
//    artis::common::observer::TimedIterator<artis::common::DoubleTime>>
//    output(rc.observer());
//
//  output(context.begin(), context.end(), {context.begin(), 0.1});
}

int main() {
  std::cout << "test_parabola START" << std::endl;
  test_parabola();
  std::cout << "test_predator_prey START" << std::endl;
  test_predator_prey();
  std::cout << "test_predator_prey_smart_gardener START" << std::endl;
  test_predator_prey_smart_gardener();
  std::cout << "test_mixed_predator_prey START" << std::endl;
  test_mixed_predator_prey();
  std::cout << "test_multi_predator_prey START" << std::endl;
  test_multi_predator_prey();
}