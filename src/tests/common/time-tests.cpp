/**
 * @file tests/common/time-tests.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <artis-star/common/time/DoubleTime.hpp>
#include <artis-star/common/time/IntegerTime.hpp>
#include <artis-star/common/time/RationalTime.hpp>

#define CATCH_CONFIG_MAIN

#include <catch2/catch_all.hpp>

using namespace artis::common;

TEST_CASE("double time", "common")
{
  artis::common::DoubleTime::type t1 = 0;
  artis::common::DoubleTime::type t2 = artis::common::DoubleTime::infinity;

  REQUIRE(t1 != artis::common::DoubleTime::infinity);
  REQUIRE(t1 == artis::common::DoubleTime::null);
  REQUIRE(t2 == artis::common::DoubleTime::infinity);
  REQUIRE(t2 != artis::common::DoubleTime::null);
}

TEST_CASE("integer time", "common")
{
  artis::common::IntegerTime::type t1 = 0;
  artis::common::IntegerTime::type t2 = artis::common::IntegerTime::infinity;

  REQUIRE(t1 != artis::common::IntegerTime::infinity);
  REQUIRE(t1 == artis::common::IntegerTime::null);
  REQUIRE(t2 == artis::common::IntegerTime::infinity);
  REQUIRE(t2 != artis::common::IntegerTime::null);
}

TEST_CASE("rational time", "common")
{
  artis::common::RationalTime::type t1 = 0;
  artis::common::RationalTime::type t2 = artis::common::RationalTime::infinity;

  REQUIRE(t1 != artis::common::RationalTime::infinity);
  REQUIRE(t1 == artis::common::RationalTime::null);
  REQUIRE(t2 == artis::common::RationalTime::infinity);
  REQUIRE(t2 != artis::common::RationalTime::null);
  REQUIRE(t1 + 1 == 1);
  REQUIRE(t1 + artis::common::RationalTime::type(1, 2) == artis::common::RationalTime::type(1, 2));
}