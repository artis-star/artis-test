/**
 * @file tests/devs/models.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTS_DEVS_MODELS_HPP
#define TESTS_DEVS_MODELS_HPP

#include <artis-star/common/time/DoubleTime.hpp>
#include <artis-star/common/utils/Trace.hpp>

#include <artis-star/kernel/devs/Dynamics.hpp>

#include <chrono>
#include <iostream>

namespace artis::tests::devs {

struct data
{
  double x;
  double y;

  data()
      : x(0), y(0)
  {}

  data(double _x, double _y)
      : x(_x), y(_y)
  {}

  bool operator==(const data &other) const {
    return x == other.x and y == other.y;
  }

  std::string to_string() const {
    return "< " + std::to_string(x) + " ; " + std::to_string(y) + " >";
  }
};

class A : public artis::devs::Dynamics<common::DoubleTime, A>
{
public:
  A(const std::string &name,
    const artis::devs::Context<common::DoubleTime, A, artis::common::NoParameters> &context)
      :
      artis::devs::Dynamics<common::DoubleTime, A>(name, context)
  {
  }

  ~A() override = default;

  void dint(typename common::DoubleTime::type t) override
  {

#ifndef WITH_TRACE
    (void)t;
#else
    common::Trace<common::DoubleTime>::trace()
        << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                    common::FormalismType::DEVS,
                                                    common::FunctionType::DELTA_INT,
                                                    common::LevelType::USER);
    common::Trace<common::DoubleTime>::trace().flush();
#endif

    if (_phase == WAIT) {
      ++_value.x;
      --_value.y;
      _phase = SEND;
    } else if (_phase == SEND) {
      _phase = WAIT;
    }
  }

  void
  dext(typename common::DoubleTime::type t, typename common::DoubleTime::type /* e */,
       const common::event::ExternalEvent<common::DoubleTime> &msg) override
  {

#ifndef WITH_TRACE
    (void)t;
    (void)msgs;
#else
    common::Trace<common::DoubleTime>::trace()
        << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                    common::FormalismType::DEVS,
                                                    common::FunctionType::DELTA_EXT,
                                                    common::LevelType::USER)
        << "event = " << msg.to_string();
    common::Trace<common::DoubleTime>::trace().flush();
#endif

    _phase = SEND;
  }

  void start(typename common::DoubleTime::type t) override
  {

#ifndef WITH_TRACE
    (void)t;
#else
    common::Trace<common::DoubleTime>::trace()
        << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                    common::FormalismType::DEVS,
                                                    common::FunctionType::START,
                                                    common::LevelType::USER);
    common::Trace<common::DoubleTime>::trace().flush();
#endif

    _phase = SEND;
  }

  typename common::DoubleTime::type
  ta(typename common::DoubleTime::type t) const override
  {

#ifndef WITH_TRACE
    (void)t;
#else
    common::Trace<common::DoubleTime>::trace()
        << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                    common::FormalismType::DEVS,
                                                    common::FunctionType::TA,
                                                    common::LevelType::USER);
    common::Trace<common::DoubleTime>::trace().flush();
#endif

    if (_phase == WAIT) {
      return 1;
    } else {
      return 0;
    }
  }

  common::event::ExternalEvent<common::DoubleTime>
  lambda(typename common::DoubleTime::type t) const override
  {
#ifndef WITH_TRACE
    (void)t;
#endif

    if (_phase == SEND) {
      artis::common::event::ExternalEvent<common::DoubleTime> msg(_value);

#ifdef WITH_TRACE
      common::Trace<common::DoubleTime>::trace()
          << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                      common::FormalismType::DEVS,
                                                      common::FunctionType::LAMBDA,
                                                      common::LevelType::USER)
          << "event = " << msg.to_string();
      common::Trace<common::DoubleTime>::trace().flush();
#endif

      return msg;
    } else {

#ifdef WITH_TRACE
      common::Trace<common::DoubleTime>::trace()
          << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                      common::FormalismType::DEVS,
                                                      common::FunctionType::LAMBDA,
                                                      common::LevelType::USER)
          << "no event";
      common::Trace<common::DoubleTime>::trace().flush();
#endif

      return artis::common::event::ExternalEvent<common::DoubleTime>::Void;
    }
  }

private:
  enum Phase
  {
    WAIT, SEND
  };

  Phase _phase;
  data _value;
};

class B : public artis::devs::Dynamics<common::DoubleTime, B>
{
public:
  B(const std::string &name,
    const artis::devs::Context<common::DoubleTime, B, artis::common::NoParameters> &context)
      :
      artis::devs::Dynamics<common::DoubleTime, B>(name, context),
      _value(0)
  {
  }

  ~B() override = default;

  void dint(typename common::DoubleTime::type t) override
  {

#ifndef WITH_TRACE
    (void)t;
#else
    common::Trace<common::DoubleTime>::trace()
        << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                    common::FormalismType::DEVS,
                                                    common::FunctionType::DELTA_INT,
                                                    common::LevelType::USER);
    common::Trace<common::DoubleTime>::trace().flush();
#endif

    if (_phase == SEND) {
      _phase = WAIT;
    }
  }

  void
  dext(typename common::DoubleTime::type t, typename common::DoubleTime::type /* e */,
       const common::event::ExternalEvent<common::DoubleTime> &msg) override
  {

#ifndef WITH_TRACE
    (void)t;
    (void)msgs;
#else
    common::Trace<common::DoubleTime>::trace()
        << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                    common::FormalismType::DEVS,
                                                    common::FunctionType::DELTA_EXT,
                                                    common::LevelType::USER)
        << "event = " << msg.to_string();
    common::Trace<common::DoubleTime>::trace().flush();
#endif

    _phase = SEND;
  }

  void start(typename common::DoubleTime::type t) override
  {

#ifndef WITH_TRACE
    (void)t;
#else
    common::Trace<common::DoubleTime>::trace()
        << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                    common::FormalismType::DEVS,
                                                    common::FunctionType::START,
                                                    common::LevelType::USER);
    common::Trace<common::DoubleTime>::trace().flush();
#endif

    _phase = WAIT;
  }

  typename common::DoubleTime::type ta(
      typename common::DoubleTime::type t) const override
  {

#ifndef WITH_TRACE
    (void)t;
#else
    common::Trace<common::DoubleTime>::trace()
        << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                    common::FormalismType::DEVS,
                                                    common::FunctionType::TA,
                                                    common::LevelType::USER);
    common::Trace<common::DoubleTime>::trace().flush();
#endif

    if (_phase == WAIT) {
      return common::DoubleTime::infinity;
    } else {
      return 0;
    }
  }

  common::event::ExternalEvent<common::DoubleTime> lambda(
      typename common::DoubleTime::type t) const override
  {
#ifndef WITH_TRACE
    (void)t;
#endif
    if (_phase == SEND) {
      artis::common::event::ExternalEvent<common::DoubleTime> msg(_value);

#ifdef WITH_TRACE
      common::Trace<common::DoubleTime>::trace()
          << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                      common::FormalismType::DEVS,
                                                      common::FunctionType::LAMBDA,
                                                      common::LevelType::USER)
          << "event = " << msg.to_string();
      common::Trace<common::DoubleTime>::trace().flush();
#endif

      return msg;
    } else {

#ifdef WITH_TRACE
      common::Trace<common::DoubleTime>::trace()
          << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                      common::FormalismType::DEVS,
                                                      common::FunctionType::LAMBDA,
                                                      common::LevelType::USER)
          << "no event";
      common::Trace<common::DoubleTime>::trace().flush();
#endif

      return artis::common::event::ExternalEvent<common::DoubleTime>::Void;
    }
  }

private:
  enum Phase
  {
    WAIT, SEND
  };

  Phase _phase;
  double _value;
};

}

// namespace artis tests devs

#endif
