/**
 * @file tests/devs/tests.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <tests/devs/graph_manager.hpp>
#include <tests/devs/models.hpp>

#include <artis-star/common/RootCoordinator.hpp>

#define CATCH_CONFIG_MAIN

#include <catch2/catch_all.hpp>

using namespace artis::tests::devs;
using namespace artis::common;

TEST_CASE("devs/flat", "run")
{
  artis::common::context::Context<artis::common::DoubleTime> context(0, 10);
  artis::common::RootCoordinator<
      DoubleTime, artis::devs::Coordinator<
          DoubleTime,
          FlatGraphManager,
          Select>
  > rc(context, "root", artis::common::NoParameters(), artis::common::NoParameters());

  artis::common::Trace<DoubleTime>::trace().clear();
  rc.run(context);

  std::cout << artis::common::Trace<DoubleTime>::trace().elements().filter_level_type(
      LevelType::USER).to_string() << std::endl;

  REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().
      filter_model_name("a1").
      filter_function_type(artis::common::FunctionType::START).size() == 1);
  REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().
      filter_model_name("b1").
      filter_function_type(artis::common::FunctionType::START).size() == 1);
  REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().
      filter_model_name("a2").
      filter_function_type(artis::common::FunctionType::START).size() == 1);
  REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().
      filter_model_name("b2").
      filter_function_type(artis::common::FunctionType::START).size() == 1);

  REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().
      filter_model_name("a1").
      filter_function_type(artis::common::FunctionType::DELTA_EXT).empty());
  // at t = 0
  REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().
      filter_model_name("a1").filter_time(0).
      filter_function_type(artis::common::FunctionType::DELTA_INT).size() == 1);
  REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().
      filter_model_name("a1").filter_time(0).
      filter_function_type(artis::common::FunctionType::TA).size() == 2);
  REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().
      filter_model_name("a1").filter_time(0).
      filter_function_type(artis::common::FunctionType::LAMBDA).size() == 1);

  // at t > 0
  for (unsigned int t = 1; t <= 10; ++t) {
    REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().
        filter_model_name("a1").filter_time(t).
        filter_function_type(artis::common::FunctionType::LAMBDA).size() == 2);
    REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().
        filter_model_name("a1").filter_time(t).
        filter_function_type(artis::common::FunctionType::DELTA_INT).size() == 2);
    REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().
        filter_model_name("a1").filter_time(t).
        filter_function_type(artis::common::FunctionType::TA).size() == 2);
  }

  for (unsigned int t = 0; t <= 10; ++t) {
    REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().
        filter_model_name("b1").filter_time(t).
        filter_function_type(artis::common::FunctionType::LAMBDA).size() == 1);
    REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().
        filter_model_name("b1").filter_time(t).
        filter_function_type(artis::common::FunctionType::DELTA_INT).size() == 1);
    if (t == 0) {
      REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().
          filter_model_name("b1").filter_time(t).
          filter_function_type(artis::common::FunctionType::TA).size() == 3);
    } else {
      REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().
          filter_model_name("b1").filter_time(t).
          filter_function_type(artis::common::FunctionType::TA).size() == 2);
    }
    REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().
        filter_model_name("b1").filter_time(t).
        filter_function_type(artis::common::FunctionType::DELTA_EXT).size() == 1);
  }

  REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().
      filter_model_name("a2").filter_function_type(
      artis::common::FunctionType::DELTA_CONF).empty());

  // at t = 0
  REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().
      filter_model_name("a2").filter_time(0).
      filter_function_type(artis::common::FunctionType::LAMBDA).size() == 2);
  REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().
      filter_model_name("a2").filter_time(0).
      filter_function_type(artis::common::FunctionType::DELTA_INT).size() == 2);
  REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().
      filter_model_name("a2").filter_time(0).
      filter_function_type(artis::common::FunctionType::TA).size() == 4);
  REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().
      filter_model_name("a2").filter_time(0).
      filter_function_type(artis::common::FunctionType::DELTA_EXT).size() == 1);

  // at t > 0
  for (unsigned int t = 1; t <= 10; ++t) {
    REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().
        filter_model_name("a2").filter_time(t).
        filter_function_type(artis::common::FunctionType::LAMBDA).size() == 3);
    REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().
        filter_model_name("a2").filter_time(t).
        filter_function_type(artis::common::FunctionType::DELTA_INT).size() == 3);
    REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().
        filter_model_name("a2").filter_time(t).
        filter_function_type(artis::common::FunctionType::TA).size() == 4);
    REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().
        filter_model_name("a2").filter_time(t).
        filter_function_type(artis::common::FunctionType::DELTA_EXT).size() == 1);
  }

  for (unsigned int t = 0; t <= 10; ++t) {
    REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().
        filter_model_name("b2").filter_time(t).
        filter_function_type(artis::common::FunctionType::LAMBDA).size() == 2);
    REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().
        filter_model_name("b2").filter_time(t).
        filter_function_type(artis::common::FunctionType::DELTA_INT).size() == 2);
    if (t == 0) {
      REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().
          filter_model_name("b2").filter_time(t).
          filter_function_type(artis::common::FunctionType::TA).size() == 5);
    } else {
      REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().
          filter_model_name("b2").filter_time(t).
          filter_function_type(artis::common::FunctionType::TA).size() == 4);
    }
    REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().
        filter_model_name("b2").filter_time(t).
        filter_function_type(artis::common::FunctionType::DELTA_EXT).size() == 2);
  }
}