/**
 * @file tests/devs/graph_manager.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTS_DEVS_GRAPH_MANAGER_HPP
#define TESTS_DEVS_GRAPH_MANAGER_HPP

#include <tests/devs/models.hpp>

#include <artis-star/kernel/devs/Coordinator.hpp>
#include <artis-star/kernel/devs/GraphManager.hpp>
#include <artis-star/kernel/devs/Simulator.hpp>

namespace artis {
namespace tests {
namespace devs {

class FlatGraphManager :
    public artis::devs::GraphManager<common::DoubleTime>
{
public:
  enum submodels
  {
    FirstA, SecondA,
    FirstB, SecondB
  };

  FlatGraphManager(common::Coordinator<common::DoubleTime> *coordinator,
                   const artis::common::NoParameters &parameters,
                   const artis::common::NoParameters &graph_parameters)
      :
      artis::devs::GraphManager<common::DoubleTime>(coordinator, parameters,
                                                    graph_parameters),
      a1("a1", parameters), b1("b1", parameters),
      a2("a2", parameters), b2("b2", parameters)
  {
    add_child(FirstA, &a1);
    add_child(FirstB, &b1);
    add_child(SecondA, &a2);
    add_child(SecondB, &b2);

    add_link(&a1, &b1);
    add_link(&b1, &a2);
    add_link(&a2, &b2);
  }

  ~FlatGraphManager() override = default;

private:
  artis::devs::Simulator<common::DoubleTime, A> a1;
  artis::devs::Simulator<common::DoubleTime, B> b1;
  artis::devs::Simulator<common::DoubleTime, A> a2;
  artis::devs::Simulator<common::DoubleTime, B> b2;
};

class Select
{
public:
  Select() = default;

  common::Model<common::DoubleTime> *
  operator()(const common::Models<common::DoubleTime> &list) const
  { return list.at(0); }
};

}
}
} // namespace artis tests devs

#endif
