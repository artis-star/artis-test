/**
 * @file tests/multithreading/simple/main.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <artis-star/common/RootCoordinator.hpp>
#include <artis-star/kernel/pdevs/multithreading/Coordinator.hpp>

#include <tests/multithreading/simple/graph_manager.hpp>
#include <tests/multithreading/simple/models.hpp>

#include <chrono>
#include <iostream>

using namespace artis::common;
using namespace std::chrono;
using namespace artis::tests::multithreading::simple;

//void simple_monothreading()
//{
//  GeneratorParameters parameters = {10, 2, 5, 763752};
//  artis::common::context::Context<artis::common::DoubleTime> context(0, 200000);
//  artis::common::RootCoordinator<
//      DoubleTime, artis::pdevs::Coordinator<
//          DoubleTime,
//          SimpleGraphManager,
//          GeneratorParameters,
//          NoParameters>
//  > rc(context, "root", parameters, NoParameters());
//
//  steady_clock::time_point t1 = steady_clock::now();
//
//  rc.run(context);
//
//  steady_clock::time_point t2 = steady_clock::now();
//
//  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);
//
//  std::cout << "Time = " << time_span.count() << std::endl;
//}

void simple_multithreading()
{
  GeneratorParameters parameters = {10, 2, 5, 763752};
  artis::common::context::Context<artis::common::DoubleTime> context(0, 200000);
  artis::common::RootCoordinator<
      DoubleTime, artis::pdevs::multithreading::Coordinator<
          DoubleTime,
          SimpleGraphManager,
          GeneratorParameters,
          NoParameters>
  > rc(context, "root", parameters, NoParameters());

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  std::cout << "Time = " << time_span.count() << std::endl;
}

int main()
{
//  simple_monothreading();
  simple_multithreading();
  return 0;
}
