/**
 * @file tests/multithreading/lifegame/main.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <artis-star/common/RootCoordinator.hpp>

#include <tests/multithreading/lifegame/graph_manager.hpp>
#include <tests/multithreading/lifegame/models.hpp>

#include <chrono>
#include <iostream>

using namespace artis::common;
using namespace std::chrono;
using namespace artis::tests::multithreading::lifegame;

CellParameters parameters({
                            {
                              {"C_1_1", 3},
                              {"C_1_2", 5},
                              {"C_1_3", 5},
                              {"C_1_4", 5},
                              {"C_1_5", 5},
                              {"C_1_6", 5},
                              {"C_1_7", 5},
                              {"C_1_8", 5},
                              {"C_1_9", 5},
                              {"C_1_10", 3},
                              {"C_2_1", 5},
                              {"C_2_2", 8},
                              {"C_2_3", 8},
                              {"C_2_4", 8},
                              {"C_2_5", 8},
                              {"C_2_6", 8},
                              {"C_2_7", 8},
                              {"C_2_8", 8},
                              {"C_2_9", 8},
                              {"C_2_10", 5},
                              {"C_3_1", 5},
                              {"C_3_2", 8},
                              {"C_3_3", 8},
                              {"C_3_4", 8},
                              {"C_3_5", 8},
                              {"C_3_6", 8},
                              {"C_3_7", 8},
                              {"C_3_8", 8},
                              {"C_3_9", 8},
                              {"C_3_10", 5},
                              {"C_4_1", 5},
                              {"C_4_2", 8},
                              {"C_4_3", 8},
                              {"C_4_4", 8},
                              {"C_4_5", 8},
                              {"C_4_6", 8},
                              {"C_4_7", 8},
                              {"C_4_8", 8},
                              {"C_4_9", 8},
                              {"C_4_10", 5},
                              {"C_5_1", 5},
                              {"C_5_2", 8},
                              {"C_5_3", 8},
                              {"C_5_4", 8},
                              {"C_5_5", 8},
                              {"C_5_6", 8},
                              {"C_5_7", 8},
                              {"C_5_8", 8},
                              {"C_5_9", 8},
                              {"C_5_10", 5},
                              {"C_6_1", 5},
                              {"C_6_2", 8},
                              {"C_6_3", 8},
                              {"C_6_4", 8},
                              {"C_6_5", 8},
                              {"C_6_6", 8},
                              {"C_6_7", 8},
                              {"C_6_8", 8},
                              {"C_6_9", 8},
                              {"C_6_10", 5},
                              {"C_7_1", 5},
                              {"C_7_2", 8},
                              {"C_7_3", 8},
                              {"C_7_4", 8},
                              {"C_7_5", 8},
                              {"C_7_6", 8},
                              {"C_7_7", 8},
                              {"C_7_8", 8},
                              {"C_7_9", 8},
                              {"C_7_10", 5},
                              {"C_8_1", 5},
                              {"C_8_2", 8},
                              {"C_8_3", 8},
                              {"C_8_4", 8},
                              {"C_8_5", 8},
                              {"C_8_6", 8},
                              {"C_8_7", 8},
                              {"C_8_8", 8},
                              {"C_8_9", 8},
                              {"C_8_10", 5},
                              {"C_9_1", 5},
                              {"C_9_2", 8},
                              {"C_9_3", 8},
                              {"C_9_4", 8},
                              {"C_9_5", 8},
                              {"C_9_6", 8},
                              {"C_9_7", 8},
                              {"C_9_8", 8},
                              {"C_9_9", 8},
                              {"C_9_10", 5},
                              {"C_10_1", 3},
                              {"C_10_2", 5},
                              {"C_10_3", 5},
                              {"C_10_4", 5},
                              {"C_10_5", 5},
                              {"C_10_6", 5},
                              {"C_10_7", 5},
                              {"C_10_8", 5},
                              {"C_10_9", 5},
                              {"C_10_10", 3}
                            },
                            {
                              {"C_1_1", false},
                              {"C_1_2", false},
                              {"C_1_3", false},
                              {"C_1_4", false},
                              {"C_1_5", false},
                              {"C_1_6", false},
                              {"C_1_7", false},
                              {"C_1_8", false},
                              {"C_1_9", false},
                              {"C_1_10", false},
                              {"C_2_1", false},
                              {"C_2_2", false},
                              {"C_2_3", false},
                              {"C_2_4", false},
                              {"C_2_5", true},
                              {"C_2_6", true},
                              {"C_2_7", false},
                              {"C_2_8", false},
                              {"C_2_9", false},
                              {"C_2_10", false},
                              {"C_3_1", false},
                              {"C_3_2", false},
                              {"C_3_3", false},
                              {"C_3_4", true},
                              {"C_3_5", false},
                              {"C_3_6", false},
                              {"C_3_7", true},
                              {"C_3_8", false},
                              {"C_3_9", false},
                              {"C_3_10", false},
                              {"C_4_1", false},
                              {"C_4_2", false},
                              {"C_4_3", true},
                              {"C_4_4", false},
                              {"C_4_5", false},
                              {"C_4_6", false},
                              {"C_4_7", false},
                              {"C_4_8", true},
                              {"C_4_9", false},
                              {"C_4_10", false},
                              {"C_5_1", false},
                              {"C_5_2", true},
                              {"C_5_3", false},
                              {"C_5_4", false},
                              {"C_5_5", false},
                              {"C_5_6", false},
                              {"C_5_7", false},
                              {"C_5_8", false},
                              {"C_5_9", true},
                              {"C_5_10", false},
                              {"C_6_1", false},
                              {"C_6_2", true},
                              {"C_6_3", false},
                              {"C_6_4", false},
                              {"C_6_5", false},
                              {"C_6_6", false},
                              {"C_6_7", false},
                              {"C_6_8", false},
                              {"C_6_9", true},
                              {"C_6_10", false},
                              {"C_7_1", false},
                              {"C_7_2", false},
                              {"C_7_3", true},
                              {"C_7_4", false},
                              {"C_7_5", false},
                              {"C_7_6", false},
                              {"C_7_7", false},
                              {"C_7_8", true},
                              {"C_7_9", false},
                              {"C_7_10", false},
                              {"C_8_1", false},
                              {"C_8_2", false},
                              {"C_8_3", false},
                              {"C_8_4", true},
                              {"C_8_5", false},
                              {"C_8_6", false},
                              {"C_8_7", true},
                              {"C_8_8", false},
                              {"C_8_9", false},
                              {"C_8_10", false},
                              {"C_9_1", false},
                              {"C_9_2", false},
                              {"C_9_3", false},
                              {"C_9_4", false},
                              {"C_9_5", true},
                              {"C_9_6", true},
                              {"C_9_7", false},
                              {"C_9_8", false},
                              {"C_9_9", false},
                              {"C_9_10", false},
                              {"C_10_1", false},
                              {"C_10_2", false},
                              {"C_10_3", false},
                              {"C_10_4", false},
                              {"C_10_5", false},
                              {"C_10_6", false},
                              {"C_10_7", false},
                              {"C_10_8", false},
                              {"C_10_9", false},
                              {"C_10_10", false}
                            }
                          });
GridGraphManagerParameters graph_parameters({1, 10, 1, 10, 1, 10, 1, 10});

class View : public artis::common::observer::View<artis::common::DoubleTime> {
public:
  View() {
    selector("Cell:state", {FlatGraphManager::CELL, ALL, Cell::STATE});
  }
};

void show_state(const artis::common::observer::View<artis::common::DoubleTime> &view) {
  std::vector<std::vector<std::vector<bool> > > states;

  for (unsigned int t = 0; t <= 11; ++t) {
    states.emplace_back(std::vector<std::vector<bool> >());
    for (unsigned int i = graph_parameters.begin_column - 1; i < graph_parameters.end_column; ++i) {
      states[t].emplace_back(std::vector<bool>());
      for (unsigned int j = graph_parameters.begin_line - 1; j < graph_parameters.end_line; ++j) {
        states[t][i].emplace_back(false);
      }
    }
  }
  for (unsigned int i = graph_parameters.begin_column - 1; i < graph_parameters.end_column; ++i) {
    for (unsigned int j = graph_parameters.begin_line - 1; j < graph_parameters.end_line; ++j) {
      std::ostringstream ss;

      ss << ":root:C_" << (i + 1) << "_" << (j + 1) << ":state";

      const ::View::Values &values = view.get("Cell:state", ss.str());

      for (const auto &value: values) {
        bool state;

        value.second(state);
        if (value.first < 11) {
          states[(int) value.first][i][j] = state;
        }
      }
    }
  }
  for (unsigned int t = 0; t <= 10; ++t) {
    for (unsigned int i = graph_parameters.begin_column - 1; i < graph_parameters.end_column; ++i) {
      for (unsigned int j = graph_parameters.begin_line - 1; j < graph_parameters.end_line; ++j) {
        std::cout << states[t][i][j] << " ";
      }
      std::cout << std::endl;
    }
    std::cout << std::endl;
  }
}

double lifegame_monothreading() {
  artis::common::context::Context<artis::common::DoubleTime> context(0, 10);
  artis::common::RootCoordinator<
    DoubleTime, artis::pdevs::Coordinator<
      DoubleTime,
      FlatGraphManager,
      CellParameters,
      GridGraphManagerParameters>
  > rc(context, "root", parameters, graph_parameters);

  rc.attachView("Matrix", new ::View());

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  show_state(rc.observer().view("Matrix"));

  return time_span.count();
}

double lifegame_multithreading() {
  artis::common::context::Context<artis::common::DoubleTime> context(0, 10);
  artis::common::RootCoordinator<
    DoubleTime, artis::pdevs::multithreading::Coordinator<
      DoubleTime,
      ParallelHierarchicalGraphManager,
      CellParameters,
      GridGraphManagerParameters>
  > rc(context, "root", parameters, graph_parameters);

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  return time_span.count();
}

int main() {
  std::cout << lifegame_monothreading() << std::endl;
  std::cout << lifegame_multithreading() << std::endl;
  return 0;
}
