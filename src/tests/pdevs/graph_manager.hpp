/**
 * @file tests/pdevs/graph_manager.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTS_PDEVS_GRAPH_MANAGER_HPP
#define TESTS_PDEVS_GRAPH_MANAGER_HPP

#include <tests/pdevs/models.hpp>

#include <artis-star/kernel/pdevs/Coordinator.hpp>
#include <artis-star/kernel/pdevs/GraphManager.hpp>
#include <artis-star/kernel/pdevs/Simulator.hpp>

namespace artis {
namespace tests {
namespace pdevs {

class S1GraphManager : public artis::pdevs::GraphManager<common::DoubleTime>
{
public:
  enum submodels
  {
    OneA, OneB
  };
  enum outputs
  {
    OUT
  };

  S1GraphManager(common::Coordinator<common::DoubleTime> *coordinator,
                 const artis::common::NoParameters &parameters,
                 const artis::common::NoParameters &graph_parameters)
      :
      artis::pdevs::GraphManager<common::DoubleTime>(coordinator, parameters, graph_parameters),
      a("a1", parameters), b("b1", parameters)
  {
    add_child(OneA, &a);
    add_child(OneB, &b);

    coordinator->output_port({OUT, "out"});

    out({&a, A::OUT}) >> in({&b, B::IN});
    out({&b, B::OUT}) >> out({coordinator, OUT});
  }

  ~S1GraphManager() override = default;

private:
  artis::pdevs::Simulator<common::DoubleTime, A> a;
  artis::pdevs::Simulator<common::DoubleTime, B> b;
};

class S2GraphManager : public artis::pdevs::GraphManager<common::DoubleTime>
{
public:
  enum submodels
  {
    OneA, OneB
  };
  enum inputs
  {
    IN
  };

  S2GraphManager(common::Coordinator<common::DoubleTime> *coordinator,
                 const artis::common::NoParameters &parameters,
                 const artis::common::NoParameters &graph_parameters)
      :
      artis::pdevs::GraphManager<common::DoubleTime>(coordinator, parameters, graph_parameters),
      a("a2", parameters), b("b2", parameters)
  {
    add_child(OneA, &a);
    add_child(OneB, &b);

    coordinator->input_port({IN, "in"});

    in({coordinator, IN}) >> in({&a, A::IN});
    out({&a, A::OUT}) >> in({&b, B::IN});
  }

  ~S2GraphManager() override = default;

private:
  artis::pdevs::Simulator<common::DoubleTime, A> a;
  artis::pdevs::Simulator<common::DoubleTime, B> b;
};

class RootGraphManager :
    public artis::pdevs::GraphManager<common::DoubleTime>
{
public:
  enum submodels
  {
    OneS1, OneS2
  };

  RootGraphManager(
      common::Coordinator<common::DoubleTime> *coordinator,
      const artis::common::NoParameters &parameters,
      const artis::common::NoParameters &graph_parameters)
      :
      artis::pdevs::GraphManager<common::DoubleTime>(coordinator, parameters, graph_parameters),
      S1("S1", parameters, graph_parameters),
      S2("S2", parameters, graph_parameters)
  {
    add_child(OneS1, &S1);
    add_child(OneS2, &S2);

    out({&S1, S1GraphManager::OUT}) >> in({&S2, S2GraphManager::IN});
  }

  ~RootGraphManager() override = default;

private:
  artis::pdevs::Coordinator<common::DoubleTime, S1GraphManager> S1;
  artis::pdevs::Coordinator<common::DoubleTime, S2GraphManager> S2;
};

template<typename M>
class OnlyOneGraphManager :
    public artis::pdevs::GraphManager<common::DoubleTime>
{
public:
  enum submodels
  {
    OneM
  };

  OnlyOneGraphManager(common::Coordinator<common::DoubleTime> *coordinator,
                      const artis::common::NoParameters &parameters,
                      const artis::common::NoParameters &graph_parameters)
      :
      artis::pdevs::GraphManager<common::DoubleTime>(coordinator, parameters, graph_parameters),
      model("a", parameters)
  {
    add_child(OneM, &model);
  }

  ~OnlyOneGraphManager() override = default;

private:
  artis::pdevs::Simulator<common::DoubleTime, M> model;
};

class FlatGraphManager :
    public artis::pdevs::GraphManager<common::DoubleTime>
{
public:
  enum submodels
  {
    FirstA, SecondA,
    FirstB, SecondB
  };

  FlatGraphManager(common::Coordinator<common::DoubleTime> *coordinator,
                   const artis::common::NoParameters &parameters,
                   const artis::common::NoParameters &graph_parameters)
      :
      artis::pdevs::GraphManager<common::DoubleTime>(coordinator, parameters, graph_parameters),
      a1("a1", parameters), b1("b1", parameters),
      a2("a2", parameters), b2("b2", parameters)
  {
    add_child(FirstA, &a1);
    add_child(FirstB, &b1);
    add_child(SecondA, &a2);
    add_child(SecondB, &b2);

    out({&a1, A::OUT}) >> in({&b1, B::IN});
    out({&b1, B::OUT}) >> in({&a2, A::IN});
    out({&a2, A::OUT}) >> in({&b2, B::IN});
  }

  ~FlatGraphManager() override = default;

private:
  artis::pdevs::Simulator<common::DoubleTime, A> a1;
  artis::pdevs::Simulator<common::DoubleTime, B> b1;
  artis::pdevs::Simulator<common::DoubleTime, A> a2;
  artis::pdevs::Simulator<common::DoubleTime, B> b2;
};

}
}
} // namespace artis tests pdevs

#endif
