/**
 * @file pdevs/tests.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <tests/pdevs/graph_manager.hpp>
#include <tests/pdevs/models.hpp>

#include <artis-star/common/RootCoordinator.hpp>

#include <fstream>
#include <boost/archive/binary_oarchive.hpp>

using namespace artis::tests::pdevs;

int main()
{
  artis::common::context::Context<artis::common::DoubleTime> context(0, 10000);
  artis::common::RootCoordinator<
      artis::common::DoubleTime, artis::pdevs::Coordinator<
          artis::common::DoubleTime,
          OnlyOneGraphManager<ThreeStateModel> >
  > rc(context, "root", artis::common::NoParameters(), artis::common::NoParameters());

  rc.run(context);
  rc.save(context);

  std::ofstream os("state");
  boost::archive::binary_oarchive oa(os);

  oa << context;
  return 0;
}