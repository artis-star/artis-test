/**
 * @file tests/dtss/tests.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <tests/dtss/graph_manager.hpp>
#include <tests/dtss/models.hpp>

#include <artis-star/common/RootCoordinator.hpp>
#include <artis-star/kernel/dtss/Policy.hpp>

#define CATCH_CONFIG_MAIN

#include <catch2/catch_all.hpp>

using namespace artis::tests::dtss;
using namespace artis::common;

TEST_CASE("dtss/only_one", "run")
{
  artis::common::context::Context<artis::common::DoubleTime> context(0, 10);
  artis::dtss::Parameters<DoubleTime> parameters = {1};
  artis::common::RootCoordinator<
      DoubleTime, artis::dtss::Coordinator<
          DoubleTime,
          artis::dtss::LastBagPolicy,
          OnlyOneGraphManager,
          artis::dtss::Parameters<DoubleTime>,
          artis::common::NoParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

  artis::common::Trace<DoubleTime>::trace().clear();
  rc.run(context);

  REQUIRE(artis::common::Trace<DoubleTime>::trace().elements().
      filter_model_name("a").
      filter_function_type(artis::common::FunctionType::START).size() == 1);
  for (double t = 0; t <= 10; ++t) {
    REQUIRE(artis::common::Trace<
                DoubleTime > ::trace().elements().
                    filter_model_name("a").filter_time(t).
                    filter_function_type(artis::common::FunctionType::TRANSITION).size() == 1);
    REQUIRE(artis::common::Trace<
                DoubleTime > ::trace().elements().
                    filter_model_name("a").filter_time(t).
                    filter_level_type(artis::common::LevelType::USER).
                    filter_function_type(artis::common::FunctionType::LAMBDA).size() == 1);
  }
}

TEST_CASE("dtss/two", "run")
{
  artis::common::context::Context<artis::common::DoubleTime> context(0, 10);
  artis::dtss::Parameters<DoubleTime> parameters = {1};
  artis::common::RootCoordinator<
      DoubleTime, artis::dtss::Coordinator<
          DoubleTime,
          artis::dtss::LastBagPolicy,
          TwoGraphManager,
          artis::dtss::Parameters<DoubleTime> >
  > rc(context, "root", parameters, artis::common::NoParameters());

  artis::common::Trace<DoubleTime>::trace().clear();
  rc.run(context);

  REQUIRE(artis::common::Trace<
              DoubleTime > ::trace().elements().
                  filter_model_name("a").
                  filter_function_type(artis::common::FunctionType::START).size() == 1);
  for (unsigned int t = 0; t <= 10; ++t) {
    REQUIRE(artis::common::Trace<
                DoubleTime > ::trace().elements().
                    filter_model_name("a").filter_time(t).
                    filter_function_type(artis::common::FunctionType::TRANSITION).size() == 1);
    REQUIRE(artis::common::Trace<
                DoubleTime > ::trace().elements().
                    filter_model_name("a").filter_time(t).
                    filter_level_type(artis::common::LevelType::USER).
                    filter_function_type(artis::common::FunctionType::LAMBDA).size() == 1);
  }

  REQUIRE(artis::common::Trace<
              DoubleTime > ::trace().elements().
                  filter_model_name("b").
                  filter_function_type(artis::common::FunctionType::START).size() == 1);
  for (unsigned int t = 0; t <= 10; ++t) {
    REQUIRE(artis::common::Trace<
                DoubleTime > ::trace().elements().
                    filter_model_name("b").filter_time(t).
                    filter_function_type(artis::common::FunctionType::TRANSITION).size() == 1);
    REQUIRE(artis::common::Trace<
                DoubleTime > ::trace().elements().
                    filter_model_name("b").filter_time(t).
                    filter_level_type(artis::common::LevelType::USER).
                    filter_function_type(artis::common::FunctionType::LAMBDA).size() == 1);
  }
}
