/**
 * @file tests/dtss/models.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTS_DTSS_MODELS_HPP
#define TESTS_DTSS_MODELS_HPP

#include <artis-star/common/time/DoubleTime.hpp>
#include <artis-star/common/utils/Trace.hpp>

#include <artis-star/kernel/dtss/Coordinator.hpp>
#include <artis-star/kernel/dtss/Dynamics.hpp>

namespace artis {
namespace tests {
namespace dtss {

class A
    : public artis::dtss::Dynamics<artis::common::DoubleTime,
                                   A,
                                   artis::dtss::Parameters<common::DoubleTime>>
{
public:
  enum outputs
  {
    OUT
  };

  A(const std::string &name,
    const artis::dtss::Context<artis::common::DoubleTime,
                               A,
                               artis::dtss::Parameters<common::DoubleTime>> &context)
      :
      artis::dtss::Dynamics<artis::common::DoubleTime,
                            A,
                            artis::dtss::Parameters<common::DoubleTime>>(
          name, context),
      _value(0)
  {
    output_ports({{OUT, "out"}});
  }

  ~A() override = default;

  void transition(const artis::common::event::Bag<artis::common::DoubleTime> & /* x */,
                  const artis::common::DoubleTime::type &t) override
  {

#ifndef WITH_TRACE
    (void)t;
#endif

#ifdef WITH_TRACE
    artis::common::Trace<artis::common::DoubleTime>::trace()
        << artis::common::TraceElement<artis::common::DoubleTime>(
            get_name(), t,
            common::FormalismType::DTSS,
            common::FunctionType::TRANSITION,
            common::LevelType::USER);
    artis::common::Trace<artis::common::DoubleTime>::trace().flush();
#endif

  }

  void start(const artis::common::DoubleTime::type &t) override
  {

#ifndef WITH_TRACE
    (void)t;
#endif

#ifdef WITH_TRACE
    artis::common::Trace<artis::common::DoubleTime>::trace()
        << artis::common::TraceElement<artis::common::DoubleTime>(
            get_name(), t,
            common::FormalismType::DTSS,
            common::FunctionType::START,
            common::LevelType::USER);
    artis::common::Trace<artis::common::DoubleTime>::trace().flush();
#endif

  }

  artis::common::event::Bag<artis::common::DoubleTime> lambda(const artis::common::DoubleTime::type &t) const override
  {

#ifndef WITH_TRACE
    (void)t;
#endif

    artis::common::event::Bag<artis::common::DoubleTime> msgs;

    msgs.push_back(artis::common::event::ExternalEvent<artis::common::DoubleTime>(OUT, _value));

#ifdef WITH_TRACE
    artis::common::Trace<artis::common::DoubleTime>::trace()
        << artis::common::TraceElement<artis::common::DoubleTime>(
            get_name(), t,
            common::FormalismType::DTSS,
            common::FunctionType::LAMBDA,
            common::LevelType::USER)
        << "messages = " << msgs.to_string();
    artis::common::Trace<artis::common::DoubleTime>::trace().flush();
#endif

    return msgs;
  }

private:
  double _value;
};

class B
    : public artis::dtss::Dynamics<artis::common::DoubleTime,
                                   B,
                                   artis::dtss::Parameters<common::DoubleTime>>
{
public:
  enum inputs
  {
    IN
  };
  enum outputs
  {
    OUT
  };

  B(const std::string &name,
    const artis::dtss::Context<artis::common::DoubleTime,
                               B,
                               artis::dtss::Parameters<common::DoubleTime>> &context)
      :
      artis::dtss::Dynamics<artis::common::DoubleTime,
                            B,
                            artis::dtss::Parameters<common::DoubleTime>>(
          name, context),
      _value(0)
  {
    input_ports({{IN, "in"}});
    output_ports({{OUT, "out"}});
  }

  ~B() override = default;

  void transition(const artis::common::event::Bag<artis::common::DoubleTime> &x,
                  const artis::common::DoubleTime::type &t) override
  {

#ifndef WITH_TRACE
    (void)x;
    (void)t;
#endif

#ifdef WITH_TRACE
    artis::common::Trace<artis::common::DoubleTime>::trace()
        << artis::common::TraceElement<artis::common::DoubleTime>(
            get_name(), t,
            common::FormalismType::DTSS,
            common::FunctionType::TRANSITION,
            common::LevelType::USER)
        << "x = " << x.to_string();
    artis::common::Trace<artis::common::DoubleTime>::trace().flush();
#endif
  }

  void start(const artis::common::DoubleTime::type &t) override
  {

#ifndef WITH_TRACE
    (void)t;
#endif

#ifdef WITH_TRACE
    artis::common::Trace<artis::common::DoubleTime>::trace()
        << artis::common::TraceElement<artis::common::DoubleTime>(
            get_name(), t,
            common::FormalismType::DTSS,
            common::FunctionType::START,
            common::LevelType::USER);
    artis::common::Trace<artis::common::DoubleTime>::trace().flush();
#endif

  }

  artis::common::event::Bag<artis::common::DoubleTime> lambda(
      const artis::common::DoubleTime::type &t) const override
  {

#ifndef WITH_TRACE
    (void)t;
#endif

    artis::common::event::Bag<artis::common::DoubleTime> msgs;

    msgs.push_back(artis::common::event::ExternalEvent<artis::common::DoubleTime>(OUT, _value));

#ifdef WITH_TRACE
    artis::common::Trace<artis::common::DoubleTime>::trace()
        << artis::common::TraceElement<artis::common::DoubleTime>(
            get_name(), t,
            common::FormalismType::DTSS,
            common::FunctionType::LAMBDA,
            common::LevelType::USER)
        << "messages = " << msgs.to_string();
    artis::common::Trace<artis::common::DoubleTime>::trace().flush();
#endif

    return msgs;
  }

private:
  double _value;
};

}
}
} // namespace artis tests dtss

#endif
