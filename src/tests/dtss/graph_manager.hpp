/**
 * @file tests/dtss/graph_manager.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTS_DTSS_GRAPH_MANAGER_HPP
#define TESTS_DTSS_GRAPH_MANAGER_HPP

#include <tests/dtss/models.hpp>

#include <artis-star/kernel/dtss/Coordinator.hpp>
#include <artis-star/kernel/dtss/GraphManager.hpp>
#include <artis-star/kernel/dtss/Simulator.hpp>

namespace artis {
namespace tests {
namespace dtss {

class OnlyOneGraphManager :
    public artis::dtss::GraphManager<common::DoubleTime,
                                     artis::dtss::Parameters<common::DoubleTime>,
                                     artis::common::NoParameters>
{
public:
  enum submodels
  {
    OneA
  };

  OnlyOneGraphManager(common::Coordinator<common::DoubleTime> *coordinator,
                      const artis::dtss::Parameters<common::DoubleTime> &parameters,
                      const artis::common::NoParameters &graph_parameters)
      :
      artis::dtss::GraphManager<common::DoubleTime,
                                artis::dtss::Parameters<common::DoubleTime>,
                                artis::common::NoParameters>(
          coordinator, parameters, graph_parameters),
      a("a", parameters, parameters.time_step)
  {
    add_child(OneA, &a);
  }

  ~OnlyOneGraphManager() override = default;

private:
  artis::dtss::Simulator<common::DoubleTime, A, artis::dtss::Parameters<common::DoubleTime>> a;
};

class TwoGraphManager :
    public artis::dtss::GraphManager<common::DoubleTime,
                                     artis::dtss::Parameters<common::DoubleTime>,
                                     artis::common::NoParameters>
{
public:
  enum submodels
  {
    OneA, OneB
  };

  TwoGraphManager(common::Coordinator<common::DoubleTime> *coordinator,
                  const artis::dtss::Parameters<common::DoubleTime> &parameters,
                  const artis::common::NoParameters &graph_parameters)
      :
      artis::dtss::GraphManager<common::DoubleTime,
                                artis::dtss::Parameters<common::DoubleTime>,
                                artis::common::NoParameters>(
          coordinator, parameters, graph_parameters),
      a("a", parameters, parameters.time_step),
      b("b", parameters, parameters.time_step)
  {
    add_child(OneA, &a);
    add_child(OneB, &b);
    out({&a, A::OUT}) >> in({&b, B::IN});
  }

  ~TwoGraphManager() override = default;

private:
  artis::dtss::Simulator<common::DoubleTime, A, artis::dtss::Parameters<common::DoubleTime>> a;
  artis::dtss::Simulator<common::DoubleTime, B, artis::dtss::Parameters<common::DoubleTime>> b;
};

}
}
} // namespace artis tests dtss

#endif
